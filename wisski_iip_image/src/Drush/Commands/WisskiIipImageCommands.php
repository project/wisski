<?php

namespace Drupal\wisski_iip_image\Drush\Commands;

use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Filesystem\Filesystem;


/**
 * A Drush commandfile.
 */
final class WisskiIipImageCommands extends DrushCommands {

  use AutowireTrait;

  /**
   * Library discovery service.
   * 
   * @var Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * Constructs a WisskiIipImageCommands object.
   */
  public function __construct(LibraryDiscoveryInterface $library_discovery) {
    $this->libraryDiscovery = $library_discovery;
    parent::__construct();
  }

  /**
   * Download and install the IIPMooViewer plugin.
   *
   * @command wisski-iip-image:iipmooviewer
   * @aliases w-imv
   */
  public function download() {
    $fs = new Filesystem();
    $path = DRUPAL_ROOT . '/libraries/iipmooviewer';

    // Create path if it doesn't exist
    // Exit with a message otherwise.
    if (!$fs->exists($path)) {
      $fs->mkdir($path);
    }
    else {
      $this->logger()->notice(dt('IIPMooViewer is already present at @path. No download required.', ['@path' => $path]));
      return;
    }

    // Download the file.
    $client = new Client();
    $destination = tempnam(sys_get_temp_dir(), 'iipmooviewer-tmp');
    try {
      $client->get('https://github.com/ruven/iipmooviewer/archive/refs/heads/master.zip', ['sink' => $destination]);
    }
    catch (RequestException $e) {
      // Remove the directory.
      $fs->remove($path);
      $this->logger()->error(dt('Drush was unable to download the IIPMooViewer library from @remote. @exception', [
        '@remote' => 'https://github.com/ruven/iipmooviewer/archive/refs/heads/master.zip',
        '@exception' => $e->getMessage(),
      ]));
      return;
    }

    // Move downloaded file.
    $fs->rename($destination, $path . '/iipmooviewer.zip');

    // Unzip the file.
    $zip = new \ZipArchive();
    $res = $zip->open($path . '/iipmooviewer.zip');
    if ($res === TRUE) {
      $zip->extractTo($path);
      $zip->close();
    }
    else {
      // Remove the directory if unzip fails and exit.
      $fs->remove($path);
      $this->logger()->error(dt('Error: unable to unzip IIPMooViewer file.', []));
      return;
    }

    // Remove the downloaded zip file.
    $fs->remove($path . '/iipmooviewer.zip');

    // Move the file.
    $fs->mirror($path . '/iipmooviewer-master', $path, NULL, ['override' => TRUE]);
    $fs->remove($path . '/iipmooviewer-master');

    // Success.
    $this->logger()->success(dt('The IIPMooViewer library has been successfully downloaded to @path.', [
      '@path' => $path,
    ]));
  }
}