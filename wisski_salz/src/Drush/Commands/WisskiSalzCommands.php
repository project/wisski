<?php

namespace Drupal\wisski_salz\Drush\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;

/**
 * A Drush commandfile.
 */
final class WisskiSalzCommands extends DrushCommands {

  use AutowireTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a WisskiSalzCommands object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    parent::__construct();
  }

  /**
   * Create a SALZ adapter.
   *
   * @command wisski-salz:create-adapter
   * @aliases ws-ca
   *
   * @option type The type of the adapter (sparql11_with_pb, gnd, geonames...).
   * @option adapter_label The diplay name of the adapter.
   * @option adapter_machine_name The machine name of the adapter (optional - will be generated).
   * @option description The description of the adapter (optional).
   * @option ts_machine_name The machine name of the SALZ engine.
   * @option ts_user The user of the SALZ engine (optional - if no authentication).
   * @option ts_password The password of the SALZ engine (optional - if no authentication).
   * @option ts_use_token Set if the adapter uses a token (optional, default=0).
   * @option ts_token The token for triplestore authentification (optional).
   * @option writable Set if the adapter is writable (optional, default=1).
   * @option preferred Set if the adapter is preferred local store (optional, default=1).
   * @option read_url The read URL of the SALZ engine.
   * @option write_url The write URL of the SALZ engine.
   * @option federatable Set if the adapter is federatable (optional, default=1).
   * @option default_graph The default graph of the SALZ engine.
   * @option same_as The sameAs property reference entities (optional, default=["http://www.w3.org/2002/07/owl#sameAs"]).
   * @option ontology_graphs Existing ontology graphs (optional).
   * @option config_json Json string with config key:value pairs.
   * @usage drush wisski-salz:create-adapter --type="sparql11_with_pb" --adapter_label="Default Adapter" --adapter_machine_name="default_adapter" --description="Default SALZ adapter" --ts_machine_name="default_ts" --ts_user="user" --ts_password="password" --writable=1 --preferred=1  --read_url="https://sparql.endpoint/repository/default" --write_url="https://sparql.endpoint/repository/default/statements" --federatable=1 --default_graph="http://example.com/" --same_as="http://www.w3.org/2002/07/owl#sameAs" --ontology_graphs="http://example.com/ontology","http://example.com/ontology2"
   * @usage drush wisski-salz:create-adapter --config_json='{"type":"sparql11_with_pb", "adapter_label":"Default Adapter", "adapter_machine_name":"default_adapter", "description":"Default SALZ adapter", "ts_machine_name":"default_ts", "ts_user":"user", "ts_password":"password", "writable":true, "preferred":true, "read_url":"https://sparql.endpoint/repository/default", "write_url":"https://sparql.endpoint/repository/default/statements", "federatable":true, "default_graph":"http://example.com/", "same_as":["http://www.w3.org/2002/07/owl#sameAs"], "ontology_graphs":["http://example.com/ontology", "http://example.com/ontology2"]}'
   *
   * @throws \Drush\Exceptions\UserAbortException
   */
  public function createAdapter(
    array $options = [
      'type' => 'sparql11_with_pb',
      "adapter_label" => NULL,
      "adapter_machine_name" => NULL,
      "description" => NULL,
      "ts_machine_name" => NULL,
      "ts_user" => NULL,
      "ts_password" => NULL,
      'ts_use_token' => 0,
      'ts_token' => '',
      "writable" => 1,
      "preferred" => 1,
      "read_url" => NULL,
      "write_url" => NULL,
      "federatable" => 1,
      "default_graph" => NULL,
      "same_as" => ['http://www.w3.org/2002/07/owl#sameAs'],
      "ontology_graphs" => [],
      "config_json" => NULL,
    ],
  ) {

    // Get the config from the command line if provided.
    if (isset($options['config_json'])) {
      try {
        $options = json_decode($options['config_json'], TRUE, 512, JSON_THROW_ON_ERROR);
        if (empty($options)) {
          throw new UserAbortException('config_json is not a valid json string');
        }
      }
      catch (\JsonException $e) {
        throw new UserAbortException('config_json is not a valid json string');
      }
    }

    // Check if the required options are set.
    $required = [
      'type',
      'adapter_label',
      'ts_machine_name',
      'read_url',
      'write_url',
      'default_graph',
    ];
    $missing = [];
    foreach ($required as $key) {
      if (empty($options[$key])) {
        $missing[] = $key;
      }
    }
    if (!empty($missing)) {
      throw new UserAbortException('The following options are required: ' . implode(', ', $missing));
    }

    // Check if the type is valid.
    if (in_array($options['type'], ['sparql11_with_pb', 'gnd', 'geonames'], TRUE) === FALSE) {
      throw new UserAbortException('Type must one of the following: sparql11_with_pb, gnd, geonames');
    }

    // Sanitize the machine name.
    foreach (['adapter_machine_name', 'ts_machine_name'] as $machineName) {
      $options[$machineName] = preg_replace(
        '/[^a-zA-Z0-9_]/', '_',
        preg_replace(
          '/\s+/', '_',
          str_replace(
            [' ', 'ä', 'ö', 'ü', 'ß', '.'],
            ['_', 'ae', 'oe', 'ue', 'ss', ''],
            mb_strtolower(
              $options[$machineName]
            )
          )
        )
      );
    }

    // Check if the machine name is valid for all machine names.
    $machineNames = ['adapter_machine_name', 'ts_machine_name'];
    foreach ($machineNames as $machineName) {
      if (preg_match('/[^a-z0-9_]/', $options[$machineName])) {
        throw new UserAbortException("The $machineName should only contain lowercase letters, numbers and underscores.");
      }
    }

    // Check if the urls are valid.
    $urls = [
      'read_url',
      'write_url',
      'default_graph',
    ];
    foreach ($urls as $url) {
      if (filter_var($options[$url], FILTER_VALIDATE_URL) === FALSE) {
        throw new UserAbortException("The $url should be a valid URL.");
      }
    }

    // Check if the same_as and ontology_graphs are valid URLs.
    foreach ($options['same_as'] as $same_as) {
      if (filter_var($same_as, FILTER_VALIDATE_URL) === FALSE) {
        throw new UserAbortException('The same_as property should only contain valid URLs.');
      }
    }
    foreach ($options['ontology_graphs'] as $ontology_graph) {
      if (filter_var($ontology_graph, FILTER_VALIDATE_URL) === FALSE) {
        throw new UserAbortException('The ontology graphs should only contain valid URLs.');
      }
    }

    // Get the storage for the wisski_salz_adapter entity type.
    /** @var \Drupal\Core\Entity\EntityStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('wisski_salz_adapter');

    // Create a new adapter entity.
    /** @var \Drupal\wisski_salz\AdapterInterface $adapter */
    $adapter = $storage->create([
      "id" => $options["adapter_machine_name"],
      "label" => $options["adapter_label"],
      "description" => $options["description"],
    ]);

    if (!empty($options["ts_user"]) && !empty($options["ts_password"])) {
      $header = $options["ts_user"] . ":" . $options["ts_password"];
      $header = base64_encode($header);
    }

    // Set the engine options for the adapter.
    $tsConfig = [
      "adapterId" => $options["adapter_machine_name"],
      "id" => $options["type"],
      "machine-name" => $options["adapter_machine_name"],
      "header" => ($header ?? ""),
      "use_token" => $options["ts_use_token"],
      "token" => $options["ts_token"],
      "writeable" => $options["writable"],
      "is_preferred_local_store" => $options["preferred"],
      "read_url" => $options["read_url"],
      "write_url" => $options["write_url"],
      "is_federatable" => $options["federatable"],
      "default_graph" => $options["default_graph"],
      "same_as_properties" => $options["same_as"],
      "ontology_graphs" => $options["ontology_graphs"],
    ];

    $adapter->setEngineConfig($tsConfig);

    // Save the adapter entity.
    $adapter->save();

    // Output a success message.
    $this->output()->writeln('SALZ adapter created successfully.');
  }

}
