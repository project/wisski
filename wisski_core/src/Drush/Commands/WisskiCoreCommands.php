<?php

namespace Drupal\wisski_core\Drush\Commands;

use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;
use Drupal\wisski_core\Controller\WisskiBundleMenuController;
use Symfony\Component\Serializer\SerializerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\wisski_core\WisskiHelper;
use Drupal\wisski_adapter_sparql11_pb\Plugin\wisski_salz\Engine\Sparql11EngineWithPB;

/**
 * A Drush commandfile.
 */
final class WisskiCoreCommands extends DrushCommands {

  use AutowireTrait;

  /**
   * WisskiBundleMenuController
   *
   * @var Drupal\wisski_core\Controller\WisskiBundleMenuController
   */
  protected $wisskiBundleContoller;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected SerializerInterface $serializer;

  /**
   * Constructs a WisskiCoreCommands object.
   */
  public function __construct(WisskiBundleMenuController $wisskiBundleContoller) {
    parent::__construct();
    $this->wisskiBundleContoller = $wisskiBundleContoller;
  }

  /**
   * Recreate WissKI menus
   *
   * @command wisski-core:recreate-menus
   * @aliases wc-rcm
   *
   * @usage drush wisski-core:recreate-menus
   */
  public function recreateMenus() {
    $this->wisskiBundleContoller->recreateMenuItems();
    $this->output()->writeln('Recreated WissKI Menus.');
  }

  /**
   * Delete one or more bundles.
   *
   * @param array $bundleIds
   *   The IDs of the bundles that should be deleted.
   * @param array $options
   *   The options.
   *
   * @option all Delete all bundles. (optional, default=FALSE).
   * @command wisski-core:delete-bundles
   *
   * @usage drush wisski-core:delete-bundles bundleId0 bundleId2 bundleId3
   *   Delete three bundles by Id.
   * @usage drush wisski-core:delete-bundles
   *   Delete all bundles.
   */
  public function deleteBundles(array $bundleIds, $options = ['all' => FALSE]) {
    if ($options['all']) {
      $bundleIds = NULL;
    } elseif (!$bundleIds) {
      $this->output()->writeln("No bundle specified. If you wish to delete all bundles use the --all option");
      return;
    }
    $bundles = \Drupal::entityTypeManager()->getStorage('wisski_bundle')->loadMultiple($bundleIds);
    foreach ($bundles as $bundle) {
      $bundle->delete();
      $this->output()->writeln($bundle->id());
    }
  }

  /**
   * Export title patterns. Be aware that this assumes operates from the root web directory.
   *
   * @param array $options
   *   The options.
   *
   * @command wisski-core:export-titlepatterns
   *
   * @option all Export all title patterns, including those of bundles that are deactivated or deleted (optional, default=FALSE).
   * @option filename The path or name to the output file (optional, default="titlepatterns").
   *
   * @usage drush wisski-core:export-titlepatterns bundleId0 bundleId2 bundleId3
   *   Export titlepatterns for thee bundles by ID.
   * @usage drush wisski-core:export-titlepatterns --all
   *   Export titlepatterns for all bundles.
   */
  public function exportTitlepatterns($options = ['all' => FALSE, 'filename' => 'titlepatterns']) {

    $title_patterns = [];
    if ($options['all']) {
      // Get all titlepatterns from state
      $title_patterns = unserialize(\Drupal::state()->get('wisski_core_title_patterns',  serialize([])));
    }
    else {
      // Get only the patterns from currently activated bundles
      /** @var \Drupal\wisski_core\Entity\WisskiBundle[] */
      $bundles = \Drupal::entityTypeManager()->getStorage('wisski_bundle')->loadMultiple();
      foreach ($bundles as $bunde) {
        $title_patterns[$bunde->id()] = $bunde->getTitlePattern();
      }
    }

    if (!$title_patterns) {
      $this->output()->writeln("No titlepatterns to export!");
      return;
    }

    $title_patterns_json = Json::encode($title_patterns);
    $filename = $options['filename'] . ".json";
    file_put_contents($filename, $title_patterns_json);
    return;
  }

  /**
   * Import title patterns. Be aware that this operates from the root web directory.
   *
   * @param array $options
   *   The options.
   *
   * @command wisski-core:import-titlepatterns
   *
   * @option all Also import the titlepatterns for bundles that are not present or deactivated in this WissKI (optional, default=False).
   * @option filename The path or name to the input file (optional, default="titlepatterns.json").
   *
   * @usage drush wisski-core:import-titlepattern --all
   *   Import all titlepatterns from "titlepatterns.json"
   * @usage drush wisski-core:import-titlepattern --filename="my_file.json"
   *   Import titlepatterns from "my_file.json"
   */
  function importTitlepatterns($options = ['all' => FALSE, 'filename' => 'titlepatterns.json']) {
    $data = file_get_contents($options['filename']);
    $title_patterns = Json::decode($data);

    // Load the bundles that are in this wisski and update their titlepatterns
    /** @var \Drupal\wisski_core\Entity\WisskiBundle[] */
    $bundles = \Drupal::entityTypeManager()->getStorage('wisski_bundle')->loadMultiple(array_keys($title_patterns));
    foreach ($bundles as $bundle) {
      $bundle->setTitlePattern($title_patterns[$bundle->id()]);
      $bundle->save();
    }

    if (!$options['all']){
      return;
    }

    # Copy over titlepatterns for bundles that are not currently in this wisski or deactivated
    $old_title_patterns = unserialize(\Drupal::state()->get('wisski_core_title_patterns', NULL) ?? serialize([]));
    foreach ($title_patterns as $bundleId => $config) {
      $old_title_patterns[$bundleId] = $config;
    }

    // For the bundles that are not in this WissKI or deactivated put them into the state
    \Drupal::state()->set('wisski_core_title_patterns', serialize($old_title_patterns));
  }

  /**
   * Load ontology into the store.
   *
   * @param array $options
   *   The options.
   *
   * @command wisski-core:import-ontology
   *
   * @option store The store to load the ontology into (required).
   * @option ontology_url The resolvable URL of the ontology (required).
   * @option reasoning Run reasoning and rebuild type and property hierarchies and domain and ranges (optional, default=FALSE).
   * @option plugin_id The id of the plugin to use (optional, default="sparql11_with_pb").
   *
   * @usage drush wisski-core:import-ontology --store="default" --ontology_url="http://wiss-ki.eu/ontology/"
   *   Imports ontology from http://wiss-ki.eu/ontology/ intostore "default".
   */
  public function importOntology(
    $options = [
      'store' => NULL,
      'ontology_url' => NULL,
      'reasoning' => FALSE,
      'plugin_id' => 'sparql11_with_pb',
    ],
  ) {
    // Delete the existing ontology.
    WisskiHelper::deleteOntology($options['store']);

    // Load the new ontology.
    WisskiHelper::loadOntologyToStore($options['store'], $options['ontology_url']);

    if ($options['reasoning']) {
      // Run reasoning.
      // Load the adapter config.
      $adapterConfig = \Drupal::config('wisski_salz.wisski_salz_adapter.' . $options['store'])->get('engine');

      // Check that the config is not empty.
      if (empty($adapterConfig)) {
        throw new \Exception("Adapter config for store '$options[store]' is empty");
      }

      // Load the plugin.
      $plugin_id = $options['plugin_id'];

      // Get the plugin definition.
      $plugin_definition = \Drupal::service('plugin.manager.wisski_salz_engine')->getDefinition($plugin_id);

      // Create the engine.
      $sparql11EngineWithPb = new Sparql11EngineWithPB($adapterConfig, $plugin_id, $plugin_definition);

      // Delete the reasoning tables.
      $sparql11EngineWithPb->prepareTables(TRUE);

      // Do the reasoning.
      $sparql11EngineWithPb->doTheReasoning();

      // Do the reasoning AGAIN!
      $sparql11EngineWithPb->doTheReasoning();
    }
  }

}
