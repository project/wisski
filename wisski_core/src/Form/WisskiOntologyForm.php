<?php

namespace Drupal\wisski_core\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\wisski_core\WisskiHelper;
use Drupal\wisski_salz\Entity\Adapter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overview form for ontology handling.
 *
 * @return form
 *   Form for the ontology handling menu
 *
 * @author Mark Fichtner
 */
class WisskiOntologyForm extends FormBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  public Connection $connection;

  /**
   * The WissKI Helper service.
   *
   * @var \Drupal\wisski_core\WisskiHelper
   */
  public WisskiHelper $wisskiHelper;

  public function __construct(Connection $connection, WisskiHelper $wisskiHelper) {
    $this->connection = $connection;
    $this->wisskiHelper = $wisskiHelper;
  }

  /**
   * The order of the containers here has to be the same as for __construct.
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('database'),
      $container->get('wisski.wisski_core.wisski_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'WisskiOntologyForm';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    // In wisski d8 there will be no local stores anymore,
    // we assume that every store could load an ontology
    // we load all store entities and
    // have to choose for which store we want to load an ontology.
    $adapters = Adapter::loadMultiple();

    $adapterlist = [];

    // Create a list of all adapters to choose from.
    /** @var \Drupal\wisski_salz\Entity\Adapter $adapter */
    foreach ($adapters as $adapter) {
      // If an adapter is not writable, it should not be allowed to load an ontology for that store.
      if ($adapter->getEngine()->isWritable()) {

        $adapterlist[$adapter->id()] = $adapter->label();
      }
    }

    // Check if there is a selected store.
    $selected_store = "";

    $selected_store = !empty($form_state->getValue('select_store')) ? $form_state->getValue('select_store') : "0";

    // Generate a select field.
    $form['select_store'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the store for which you want to load an ontology.'),
      '#default_value' => $selected_store,
      '#options' => array_merge(["0" => 'Please select.'], $adapterlist),
      '#ajax' => [
        'callback' => 'Drupal\wisski_core\Form\WisskiOntologyForm::ajaxStores',
        'wrapper' => 'select_store_div',
        'event' => 'change',
        // 'effect' => 'slide',
      ],
    ];

    // Ajax wrapper.
    $form['stores'] = [
      '#type' => 'markup',
      // The prefix/suffix provide the div that we're replacing, named by
      // #ajax['wrapper'] below.
      '#prefix' => '<div id="select_store_div">',
      '#suffix' => '</div>',
      '#value' => "",
    ];

    // If there is already a store selected.
    if (!empty($form_state->getValue('select_store'))) {

      // If there is a selected store - check if there is an ontology in the
      // store.
      $selected_id = $form_state->getValue('select_store');
      $selected_name = $adapterlist[$selected_id];
      // Load the store adapter entity object by means of the id of the selected
      // store.
      $selected_adapter = Adapter::load($selected_id);
      // drupal_set_message('Current selected adapter: ' . serialize($selected_adapter));
      // Load the engine of the adapter.
      /** @var \Drupal\wisski_adapter\WisskiSalzEngineInterface $engine */
      $engine = $selected_adapter->getEngine();

      // If the engine is of type sparql11_with_pb we can load the existing
      // ontologies.
      if ($engine->supportsOntology()) {

        // drupal_set_message('Type: ' . $engine->getPluginId());
        $infos = $engine->getOntologies();
        // drupal_set_message(serialize($infos));
        // dpm($infos);
        // There already is an ontology.
        if (!empty($infos) && count($infos) > 0) {
          $form['stores']['header'] = [
            '#type' => 'item',
            '#markup' => '<b>Currently loaded Ontology:</b><br/>',
          ];

          // MyFi: we remodel the table structure to generate a more dynamica
          // one.
          foreach ($infos as $ont) {
            $tableOntInfo[] = [$ont->ont, $ont->iri, $ont->ver, $ont->graph];
          }
          $form['stores']['newTable'] = [
            '#type' => 'table',
            '#header' => ['Name', 'Iri', 'Version', 'Graph'],
            '#rows' => $tableOntInfo,
          ];

          // MyFi: add some whitespace underneath the "Delete Ontology"
          // button to make the table more beautiful.
          $form['stores']['wrapper'] = [
            '#type' => 'container',
            '#attributes' => ['style' => "padding-bottom:10px"],
            // '#attributes' => array('class' => "pb-4"),
          ];

          $form['stores']['wrapper']['delete_ont'] = [
            '#type' => 'submit',
            '#button_type' => 'primary',
            '#name' => 'Ontology',
            '#value' => ' Delete Ontology',
            '#submit' => [[$this, 'deleteOntology']],
          ];

        }
        else {
          // No ontology was found.
          $form['stores']['load_onto'] = [
            '#type' => 'textfield',
            '#title' => "Load Ontology for store <em> $selected_name </em>:",
            '#description' => $this->t('Please give the URL to a loadable ontology.'),
          ];

          $form['stores']['load_onto_submit'] = [
            '#type' => 'submit',
            '#name' => 'Load Ontology',
            '#value' => 'Load Ontology',
            // '#submit' => array('wisski_core_load_ontology'),
          ];
        }
      }
    }

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public static function ajaxStores(array $form, FormStateInterface $form_state) {
    // dpm("yay!");.
    return $form['stores'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // drupal_set_message('hello');.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // If there is a selected store - check if there is an ontology in the
    // store.
    $selected_id = $form_state->getValue('select_store');

    // Ontology URL.
    $ontologyUrl = $form_state->getValue('load_onto');

    $this->wisskiHelper->loadOntologyToStore($selected_id, $ontologyUrl) ? $form_state->setRebuild() : $this->messenger()->addMessage($this->t('The selected store does not support the loading of ontologies.'));
  }

  /**
   * Deletes all namespaces.
   *
   * This function deletes all namespaces stored
   * within the corresponding table ("wisski_core_ontology_namespaces").
   */
  public function deleteAllNamespaces(array &$form, FormStateInterface $form_state) {
    $query = $this->connection->truncate("wisski_core_ontology_namespaces")->execute();
    return $query;
  }

  /**
   * Deletes a namespaces from ontology namespaces.
   */
  public function deleteSingleNamespace(array &$form, FormStateInterface $form_state) {
    // $query = $this->connection->truncate("wisski_core_ontology_namespaces")->execute();
    // return $query;
  }

  /**
   * This function deletes all namespaces.
   *
   * Stored within the corresponding table ("wisski_core_ontology_namespaces").
   *
   * @throws \Exception
   */
  public function deleteOntology(array &$form, FormStateInterface $form_state) {
    $selected_id = $form_state->getValue('select_store');
    $this->wisskiHelper->deleteOntology($selected_id) ? $form_state->setRebuild() : $this->messenger()->addMessage($this->t('Could not delete the ontology from the selected store.'));
  }

}
