<?php

namespace Drupal\wisski_adapter_gnd_new;

/**
 * Provides an interface for wisski_adapter_gnd_new constants.
 */
interface WisskiAdapterGndInterface {
  /**
   * The description of the constant.
   */
  const DATABASE_TABLE = "wisski_adapter_gnd_new";
  const CACHE_TABLE = "wisski_adapter_gnd_new";
  const MODULE_NAME = "wisski_adapter_gnd_new"; // todo: is there a better way to get this info?
  const AUTH_URI_PATTERN = "/^https?:\/\/d-nb\.info\/gnd\//";
  const DEBUGGING = FALSE;
  const DEBUGGING_MODULE_PREFIX = "GND module";
  const DEBUGGING_ENGINE_PREFIX = "GND engine";
}
