# WissKI authority file handling (GND)

This module provides an adapter to fetch information from the remote
authority file "Gemeinsame Normdatei" (GND) provided by the "Deutsche
Nationalbibliothek" (DNB), [DNB
GND](https://www.dnb.de/DE/Professionell/Standardisierung/GND/gnd_node.html).

## Installation

Tic `Manage` &rarr; `Extend` &rarr; `WissKI DNB GND Adapter
(new)`.

In order to use the functionality you'll need to create an adapter and
a pathbuilder, see Section [Prerequisites](#prerequisites) below.

## Deinstallation

Navigate to `Manage` &rarr; `Configuration` &rarr; `WissKI Salz
Adapters` and remove the "DNB GND (new)" adapter if
applicable. (Beforehand confirm that no pathbuilder is using this
adapter.)

Subsequently, remove checkmark at `Manage` &rarr; `Extend` &rarr;
`WissKI DNB GND Adapter (new)`.

## Prerequisites

You'll need to create an "adapter" and a "pathbuilder", see
documentation at
[wiss-ki.eu/documentation](https://wiss-ki.eu/documentation/authority-files).

Navigate to `Manage` &rarr; `Configuration` &rarr; `WissKI Salz
Adapters`, click `+Add WissKI Salz Adapter` and choose "DNB GND
(new)". At your GND pathbuilder, choose this adapter at "Which adapter
does this Pathbuilder belong to?".

## Configuration

Navigate to `.../admin/config/wisski/wisski_adapter_gnd_new/settings`
in your Browser. Depending on your pathbuilder modelling insert the
IDs of the bundles and fields in the text area "Bundle Field" or
"Bundle Entity_Reference Target_Bundle Field".

Example:

```
Bundle Field
b5575814c61abfd75465e33a3ed85811 f2ad808b44f0d9aeef509b88cc27ed3f
```

There is a bundle with the internal ID
"b5575814c61abfd75465e33a3ed85811" that you want to provide authority
data for. The internal ID "f2ad808b44f0d9aeef509b88cc27ed3f" denotes
the field where the users enter the authority file URI, for example
"https://d-nb.info/gnd/4006510-8" for the place "Bielefeld".

```
Bundle Entity_Reference Target_Bundle Field
b5575814c61abfd75465e33a3ed85811 f3340ca5a9b1bd270bd34d1b638bd861 b9ff625051225cac869640a0ba9db7c3 fc9352875e933b67c105a7a5af2e88a5
```

There is a bundle with the internal ID
"b5575814c61abfd75465e33a3ed85811" that you want to provide authority
data for. The internal ID "f3340ca5a9b1bd270bd34d1b638bd861" denotes
an **Entity Reference** field that points to another bundle with the
internal ID "b9ff625051225cac869640a0ba9db7c3". This bundle, again,
has a field with the internal ID "fc9352875e933b67c105a7a5af2e88a5"
where the users enter the authority file URI.

## Usage

If you model the fields in the authority pathbuilder that contain the
data fetched from the authority file provider, these fields will be
automatically filled in the view of the respective data record.
