<?php

namespace Drupal\wisski_adapter_geonames\Plugin\wisski_salz\Engine;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\wisski_adapter_geonames\Query\Query;
use Drupal\wisski_pathbuilder\Entity\WisskiPathEntity;
use Drupal\wisski_pathbuilder\PathbuilderEngineInterface;
use Drupal\wisski_salz\AdapterHelper;
use Drupal\wisski_salz\NonWritableEngineBase;
use EasyRdf\Graph as EasyRdf_Graph;
use EasyRdf\Literal as EasyRdf_Literal;
use EasyRdf\RdfNamespace as EasyRdf_Namespace;

/**
 * Wiki implementation of an external entity storage client.
 *
 * @Engine(
 *   id = "geonames",
 *   name = @Translation("Geonames"),
 *   description = @Translation("Provides access to Geonames")
 * )
 */
class GeonamesEngine extends NonWritableEngineBase implements PathbuilderEngineInterface {

  /**
   * The URI pattern for Geonames URIs.
   *
   * @var string
   */
  protected $uriPattern = "!^https://sws.geonames.org/(\w+)/$!u";
  /**
   * The template for fetching data from Geonames.
   *
   * @var string
   */
  protected $fetchTemplate = "http://sws.geonames.org/{id}/about.rdf";

  /**
   * Workaround for super-annoying easyrdf buggy behavior.
   *
   * It will only work on prefixed properties.
   *
   * @var array
   */
  protected $rdfNamespaces = [
    'gn' => 'http://www.geonames.org/ontology#',
    'wgs84' => 'http://www.w3.org/2003/01/geo/wgs84_pos#',
    'gsp' => '<http://www.opengis.net/ont/geosparql#>',
  ];

  /**
   * The possible steps for Geonames.
   *
   * @var array
   */
  protected $possibleSteps = [
    'gn:Feature' => [
      'gn:name' => NULL,
      'gn:alternateName' => NULL,
      'wgs84:lat' => NULL,
      'wgs84:long' => NULL,
      'gsp:asWKT' => NULL,
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function hasEntity($entity_id) {
    $uris = AdapterHelper::doGetUrisForDrupalIdAsArray($entity_id);
    if (empty($uris)) {
      return FALSE;
    }
    foreach ($uris as $uri) {
      // fetchData also checks if the URI matches the GND URI pattern
      // and if so tries to get the data.
      if ($this->fetchData($uri)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchData($uri = NULL, $id = NULL) {
    if (!$id) {
      if (!$uri) {
        return FALSE;
      }
      elseif (preg_match($this->uriPattern, $uri, $matches)) {
        $id = $matches[1];
      }
      else {
        // Not a URI.
        return FALSE;
      }
    }

    $cache = \Drupal::cache('wisski_adapter_geonames');
    $data = $cache->get($id);

    if ($data) {
      return $data->data;
    }

    $replaces = [
      '{id}' => $id,
    ];

    $fetchUrl = strtr($this->fetchTemplate, $replaces);

    $data = file_get_contents($fetchUrl);

    if ($data === FALSE || empty($data)) {
      return FALSE;
    }

    $graph = new EasyRdf_Graph($fetchUrl, $data, 'rdfxml');

    if ($graph->countTriples() == 0) {
      return FALSE;
    }

    foreach ($this->rdfNamespaces as $prefix => $ns) {
      EasyRdf_Namespace::set($prefix, $ns);
    }

    $data = [];

    foreach ($this->possibleSteps as $concept => $rdfPropertyChains) {
      foreach ($rdfPropertyChains as $propChain => $tmp) {
        $pChain = explode(' ', $propChain);
        $dtProp = NULL;

        if ($tmp === NULL) {
          // Last property is a datatype property.
          $dtProp = array_pop($pChain);
        }

        $resources = [$uri => $uri];

        foreach ($pChain as $prop) {
          $newResources = [];

          foreach ($resources as $resource) {
            foreach ($graph->allResources($resource, $prop) as $r) {
              $newResources[$r] = $r;
            }
          }

          $resources = $newResources;
        }

        if ($dtProp) {
          if ($dtProp == 'gsp:asWKT') {
            continue;
          }
          foreach ($resources as $resource) {
            foreach ($graph->all($resource, $dtProp) as $thing) {
              if ($thing instanceof EasyRdf_Literal) {
                $data[$concept][$propChain][] = $thing->getValue();
              }
            }
          }
        }
      }
    }

    if (!empty($data[$concept]['wgs84:lat']) && !empty($data[$concept]['wgs84:long'])) {
      $data[$concept]['gsp:asWKT'][] = 'POINT(' . $data[$concept]['wgs84:long'][0] . ' ' . $data[$concept]['wgs84:lat'][0] . ')';
    }

    $cache->set($id, $data);

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function checkUriExists($uri) {
    return !empty($this->fetchData($uri));
  }

  /**
   * {@inheritdoc}
   */
  public function createEntity($entity) {
  }

  /**
   * {@inheritdoc}
   */
  public function getBundleIdsForEntityId($id) {
    $uri = $this->getUriForDrupalId($id);
    $data = $this->fetchData($uri);
    $pbs = $this->getPbsForThis();
    $bundle_ids = [];

    foreach ($pbs as $key => $pb) {
      $groups = $pb->getMainGroups();

      foreach ($groups as $group) {
        $path = $group->getPathArray();

        if (isset($data[$path[0]])) {
          $bid = $pb->getPbPath($group->getID())['bundle'];
          $bundle_ids[] = $bid;
        }
      }
    }
    return $bundle_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function loadFieldValues(array $entity_ids = NULL, array $field_ids = NULL, $bundle = NULL, $language = LanguageInterface::LANGCODE_DEFAULT) {
    if (!$entity_ids) {
      // @todo get all entities
      $entity_ids = [];
    }

    $out = [];
    $bundleid_in = NULL;

    foreach ($entity_ids as $eid) {
      foreach ($field_ids as $fkey => $fieldid) {
        $got = $this->loadPropertyValuesForField($fieldid, [], $entity_ids, $bundleid_in);

        if (empty($out)) {
          $out = $got;
        }
        else {
          foreach ($got as $eid => $value) {
            if (empty($out[$eid])) {
              $out[$eid] = $got[$eid];
            }
            else {
              $out[$eid] = array_merge($out[$eid], $got[$eid]);
            }
          }
        }
      }
    }
    return $out;
  }

  /**
   * {@inheritdoc}
   */
  public function loadPropertyValuesForField($field_id, array $property_ids, array $entity_ids = NULL, $bundleid_in = NULL) {

    $main_property = FieldStorageConfig::loadByName('wisski_individual', $field_id);
    if (!empty($main_property)) {
      $main_property = $main_property->getMainPropertyName();
    }

    if (!empty($field_id) && empty($bundleid_in)) {
      $this->messenger()->addError("Es wurde $field_id angefragt und bundle ist aber leer.");
      return;
    }

    $pbs = [$this->getPbForThis()];
    $paths = [];

    foreach ($pbs as $key => $pb) {
      if (!$pb) {
        continue;
      }
      $field = $pb->getPbEntriesForFid($field_id);

      if (is_array($field) && !empty($field['id'])) {
        $paths[] = WisskiPathEntity::load($field["id"]);
      }
    }

    $out = [];

    foreach ($entity_ids as $eid) {

      if ($field_id == "eid") {
        $out[$eid][$field_id] = [$eid];
      }
      elseif ($field_id == "name") {
        // Tempo hack.
        $out[$eid][$field_id] = [$eid];
        continue;
      }
      elseif ($field_id == "bundle") {

        // Bundle is a special case.
        // If we are asked for a bundle, we first look in the pb cache for the bundle
        // because it could have been set by
        // measures like navigate or something - so the entity is always displayed in
        // a correct manor.
        // If this is not set we just select the first bundle that might be appropriate.
        // We select this with the first field that is there. @TODO:
        // There might be a better solution to this.
        // e.g. knowing what bundle was used for this id etc...
        // however this would need more tables with mappings that will be slow in case
        // of a lot of data...
        if (!empty($bundleid_in)) {
          $out[$eid]['bundle'] = [$bundleid_in];
          continue;
        }
        else {
          // If there is none return NULL.
          $out[$eid]['bundle'] = NULL;
          continue;
        }
      }
      else {
        if (empty($paths)) {
        }
        else {
          foreach ($paths as $key => $path) {
            $values = $this->pathToReturnValue($path, $pbs[$key], $eid, 0, $main_property);
            if (!empty($values)) {
              $uniqueValues = array_map("unserialize", array_unique(array_map("serialize", $values)));
              // Geonames returns values for each language, but we want to display all of them,
              // so we just add them all to the array with the language /"und/".
              $out[$eid][$field_id]['und'] = $uniqueValues;
            }
          }
        }
      }
    }
    return $out;
  }

  /**
   * {@inheritdoc}
   */
  public function pathToReturnValue($path, $pb, $eid = NULL, $position = 0, $main_property = NULL) {
    $uri = AdapterHelper::getUrisForDrupalId($eid, $this->adapterId());
    $data = $this->fetchData($uri);
    if (!$data) {
      return [];
    }
    $path_array = $path->getPathArray();
    $path_array[] = $path->getDatatypeProperty();
    $data_walk = $data;
    do {
      $step = array_shift($path_array);
      if (isset($data_walk[$step])) {
        $data_walk = $data_walk[$step];
      }
      else {
        // Go to the next path.
        continue;
      }
    } while (!empty($path_array));
    // Now data_walk contains only the values.
    $out = [];
    foreach ($data_walk as $value) {
      if (empty($main_property)) {
        $out[] = $value;
      }
      else {
        $out[] = [$main_property => $value];
      }
    }
    return $out;
  }

  /**
   * {@inheritdoc}
   */
  public function getPathAlternatives($history = [], $future = []) {
    if (empty($history)) {
      $keys = array_keys($this->possibleSteps);
      return array_combine($keys, $keys);
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPrimitiveMapping($step) {
    $keys = array_keys($this->possibleSteps[$step]);
    return array_combine($keys, $keys);
  }

  /**
   * {@inheritdoc}
   */
  public function getStepInfo($step, $history = [], $future = []) {
    return [$step, ''];
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryObject(EntityTypeInterface $entity_type, $condition, array $namespaces) {
    return new Query($entity_type, $condition, $namespaces);
  }

  /**
   * {@inheritdoc}
   */
  public function providesDatatypeProperty() {
    return TRUE;
  }

}
