<?php

namespace Drupal\wisski_adapter_getty_new;

/**
 * Provides an interface for wisski_adapter_getty_new constants.
 */
interface WisskiAdapterGettyInterface {
  /**
   * The description of the constant.
   */
  const DATABASE_TABLE = "wisski_adapter_getty_new";
  const CACHE_TABLE = "wisski_adapter_getty_new";
  const MODULE_NAME = "wisski_adapter_getty_new"; // todo: is there a better way to get this info?
  const AUTH_URI_PATTERN = "/^https?:\/\/vocab\.getty\.edu\/aat\//";
  const DEBUGGING = FALSE;
  const DEBUGGING_MODULE_PREFIX = "Getty AAT module";
  const DEBUGGING_ENGINE_PREFIX = "Getty AAT engine";
}
