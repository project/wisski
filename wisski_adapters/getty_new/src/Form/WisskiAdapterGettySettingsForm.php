<?php

namespace Drupal\wisski_adapter_getty_new\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

// depencency injection
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\State\State;

/**
 * Configure settings for accessing authority data (Getty AAT).
 */
 class WisskiAdapterGettySettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'wisski_adapter_getty_new.settings';

  /**
   * The Drupal messenger
   *
   * @var Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The Drupal state
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  public function __construct( 
      Messenger $messenger, 
      State $state,
      ) {
    $this->messenger = $messenger;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
      ContainerInterface $container,
      ) {
    return new static(
        $container->get('messenger'),
        $container->get('state'),
    );
  }

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return self::class;
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state){
    $config = $this->config(static::SETTINGS);

    $form['textinput_direct'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Bundle Field (pro Zeile)'),
      '#default_value' => $config->get('direct_field'),
    ];

    $form['textinput_entref'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#title' => $this->t('Bundle Entity_Reference Target_Bundle Field (pro Zeile)'),
      '#default_value' => $config->get('entref_field'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // get bundle_id(s) and field_id(s) from config
    $bundles_and_fields = $this->parseMap($form_state->getValue('textinput_direct'));
    //dpm($bundles_and_fields, "datenstruktur aus parseMap (direktes Feld)");
    $bundles_and_fields_entref = $this->parseMap($form_state->getValue('textinput_entref'));
    //dpm($bundles_and_fields_entref, "datenstruktur aus parseMap (Entity Reference)");

    // Save the configuration.
    $this->config(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('direct_field', $form_state->getValue('textinput_direct'))
      ->set('entref_field', $form_state->getValue('textinput_entref'))
      ->set('bundles_and_fields', $bundles_and_fields)
      ->set('bundles_and_fields_entref', $bundles_and_fields_entref)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Helper function that parses the textual patterns into an array
   *
   * @return a possibly empty array or NULL on failure
   */
  protected function parseMap($lines) {
    $lines = explode("\r\n", trim($lines));
    if (empty($lines)) {
      return [];
    }
    foreach ($lines as $line) {
      $line = trim($line ?? '');
      if (empty($line)) {
        continue;
      }
      // entity reference?
      elseif (preg_match('/^(?<bid>\S+)\s+(?<fidentref>\S+)\s+(?<target>\S+)\s+(?<fid>\S+)/u', $line, $matches)) {
        $fields[$matches['bid']][$matches['fidentref']][$matches['target']] = $matches['fid'];
      }
      // direct field
      elseif (preg_match('/^(?<bid>\S+)\s+(?<fid>\S+)/u', $line, $matches)) {
        $fields[$matches['bid']] = $matches['fid'];
      }
      else {
        return NULL;
      }
    }
    return $fields;
  }

}
