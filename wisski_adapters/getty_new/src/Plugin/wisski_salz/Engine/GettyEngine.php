<?php

/**
* @file
* Contains Drupal\wisski_adapter_getty_new\Plugin\wisski_salz\Engine\GettyEngine.
*/

namespace Drupal\wisski_adapter_getty_new\Plugin\wisski_salz\Engine;

use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\wisski_adapter_getty_new\Query\Query;
use Drupal\wisski_pathbuilder\Entity\WisskiPathbuilderEntity;
use Drupal\wisski_pathbuilder\Entity\WisskiPathEntity;
use Drupal\wisski_pathbuilder\PathbuilderEngineInterface;
use Drupal\wisski_salz\NonWritableEngineBase;
use Drupal\wisski_salz\AdapterHelper;
use Drupal\wisski_adapter_getty_new\WisskiAdapterGettyInterface;
use DOMDocument;
use EasyRdf\Graph as EasyRdf_Graph;
use EasyRdf\RdfNamespace as EasyRdf_Namespace;
use EasyRdf\Literal as EasyRdf_Literal;

/**
* Wisski implementation of an external entity storage client.
*
* @Engine(
*   id = "getty_new",
*   name = @Translation("Getty AAT (new)"),
*   description = @Translation("Provides access to the Getty Arts and Architecture Thesaurus.")
* )
*/
class GettyEngine extends NonWritableEngineBase implements PathbuilderEngineInterface {

protected $uriPattern  = "!^http[s]*://vocab.getty.edu/aat/(.+)$!u";
protected $fetchTemplate = "https://vocab.getty.edu/aat/{id}.ttl";
protected $debugging = WisskiAdapterGettyInterface::DEBUGGING;
protected $debug_prefix = WisskiAdapterGettyInterface::DEBUGGING_ENGINE_PREFIX;

/**
 * Workaround for super-annoying easyrdf buggy behavior:
 * it will only work on prefixed properties
 */
protected $rdfNamespaces = array(
  'aat' => 'http://vocab.getty.edu/aat/',
  'gvp' => 'http://vocab.getty.edu/ontology#',
  'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
    'sf' => 'http://www.opengis.net/ont/sf#',    
    'skos' => 'http://www.w3.org/2004/02/skos/core#',
  );

  protected $possibleSteps = array(
      'Concept' => array(
        'skos:prefLabel' => NULL,
        'gvp:parentString' => NULL,
        'rdfs:label' => NULL,
      ),
     'Type_of_acquisition' => array(
        'skos:prefLabel' => NULL,
        'gvp:parentString' => NULL,
        'rdfs:label' => NULL,
      ),
     'Genre' => array(
        'skos:prefLabel' => NULL,
        'gvp:parentString' => NULL,
        'rdfs:label' => NULL,
      ),
     'Classification' => array(
        'skos:prefLabel' => NULL,
        'gvp:parentString' => NULL,
        'rdfs:label' => NULL,
      ),
     'Material' => array(
        'skos:prefLabel' => NULL,
        'gvp:parentString' => NULL,
        'rdfs:label' => NULL,
      ),
     'Object_type' => array(
        'skos:prefLabel' => NULL,
        'gvp:parentString' => NULL,
        'rdfs:label' => NULL,
      ),
     'Price_type' => array(
        'skos:prefLabel' => NULL,
        'gvp:parentString' => NULL,
        'rdfs:label' => NULL,
      ),
     'Technique' => array(
        'skos:prefLabel' => NULL,
        'gvp:parentString' => NULL,
        'rdfs:label' => NULL,
      ),
     'Attribution_type' => array(
        'skos:prefLabel' => NULL,
        'gvp:parentString' => NULL,
        'rdfs:label' => NULL,
      ),
  );

  /**
   * {@inheritdoc}
   */
  public function hasEntity($entity_id) {
    // when debugging: get backtrace
    if ($this->debugging) {
      $backtrace = debug_backtrace( DEBUG_BACKTRACE_IGNORE_ARGS, 7 );
      $bmessage = "(EID " . $entity_id . ", Aufruf: " . $backtrace[1]['function'] . " <- " . $backtrace[2]['function'] . " <- " . $backtrace[3]['function'] . " <- " . $backtrace[4]['function'] . " <- " . $backtrace[5]['function'] . " <- " . $backtrace[6]['function'];
    }
    // simple database lookup for EID
    $database = \Drupal::database();
    $query = $database->query("SELECT eid FROM {" . WisskiAdapterGettyInterface::DATABASE_TABLE ."} WHERE eid = :entity_id",
       [':entity_id' => $entity_id,]);
    $result = $query->fetchAll();

    // if there's a match in the database this entity has an authority URI
    if (empty($result)) {
      if ($this->debugging) {
        $this->messenger()->addMessage(t("['%pref'] hasEntity FALSE ('%mess')", ['%pref' => $this->debug_prefix, '%mess' => $bmessage]));
      }
      return FALSE;
    }
    else {
      if ($this->debugging) {
        $this->messenger()->addMessage(t("['%pref'] hasEntity TRUE ('%mess')", ['%pref' => $this->debug_prefix, '%mess' => $bmessage]));
      }
      return TRUE;
    }
  }

  public function fetchData($uri = NULL, $id = NULL) {
    // only fetch data if id is set and uri fits into the authority URI pattern!
    if (!$id) {
      if (!$uri) {
        return FALSE;
      } elseif (preg_match($this->uriPattern, $uri, $matches)) {
        $id = $matches[1];
      } else {
        // not an authority URI
        return FALSE;
      }
    }

    // take data from cache if available
    $cache = \Drupal::cache(WisskiAdapterGettyInterface::CACHE_TABLE);
    $data = $cache->get($id);
    #dpm($data, "from cache?");

    if (isset($data) && !empty($data->data)) {
      return $data->data;
    }

    // if data not available in cache fetch it from remote source

    // get all language codes used in this instance
    $available_languages = \Drupal::languageManager()->getLanguages();
    $available_languages = array_keys($available_languages);
    // add default langcode "en" if not contained in available languages (fallback)
    if (!in_array("en", $available_languages)) {
      $available_languages[] = "en";
    }
    $langcode = \Drupal::service('language_manager')->getCurrentLanguage()->getId();

    $replaces = array(
      '{id}' => $id,
    );
    $fetchUrl = strtr($this->fetchTemplate, $replaces);
    if ($this->debugging) {
      $this->messenger()->addMessage(t("['%pref'] fetchUrl: '%furl' (für id '%id', current language '%curr', available languages '%avail')", ['%pref' => $this->debug_prefix, '%furl' => $fetchUrl, '%id' => $id, '%curr' => serialize($langcode), '%avail' => serialize($available_languages)]));
    }

    $opts = [
      "http" => [
        "method" => "GET",
        "header" => "Accept: application/turtle\r\n"
      ]
    ];

    $context = stream_context_create($opts);
    
    // fetch data from remote site into array structure
    $data = file_get_contents($fetchUrl, false, $context);

    if ($data === FALSE || empty($data)) {
      return FALSE;
    }

    // get relevant information from array (preferred label (english), generic terms)
    // http://vocab.getty.edu/aat/300011154 --> http://www.w3.org/2008/05/skos-xl#prefLabel
    // --> http://vocab.getty.edu/aat/term/1000011154-en
    //     http://vocab.getty.edu/aat/term/1000011154-en --> http://vocab.getty.edu/ontology#term --> "aventurine"
    //     http://vocab.getty.edu/aat/term/1000011154-en --> http://vocab.getty.edu/ontology#qualifier --> "quartz"
    // ( http://vocab.getty.edu/aat/300388277: "English (language)" )

    // relevant 'predicates': 'http://vocab.getty.edu/ontology#parentString', 
    // 'http://vocab.getty.edu/ontology#parentStringAbbrev', 'http://www.w3.org/2004/02/skos/core#prefLabel', 
    // 'http://www.w3.org/2004/02/skos/core#altLabel', 

#    dpm($fetchUrl, "fu?");
    $graph = new EasyRdf_Graph($fetchUrl, $data, 'turtle');
#    dpm($graph, "graph?");
    if ($this->debugging) {
      $this->messenger()->addMessage(t("['%pref'] Anzahl geholter Triples: '%cnt'", ['%pref' => $this->debug_prefix, '%cnt' => $graph->countTriples()]));
    }
    if ($graph->countTriples() == 0) {
      return FALSE;
    }

    foreach ($this->rdfNamespaces as $prefix => $ns) {
      EasyRdf_Namespace::set($prefix, $ns);
    }

    $data = array();

    // Property Chains don't work with unnamed bnodes :/
    foreach ($this->possibleSteps as $concept => $rdfPropertyChains) {
      if ($this->debugging) {
#        dpm($concept, "con?");
#        dpm($rdfPropertyChains, "rdf?");
        $this->messenger()->addMessage(t("['%pref'] concept: '%con'", ['%pref' => $this->debug_prefix, '%con' => serialize($concept)]));
        $this->messenger()->addMessage(t("['%pref'] rdfPropertyChains: '%prop'", ['%pref' => $this->debug_prefix, '%prop' => serialize($rdfPropertyChains)]));
      }
      foreach ($rdfPropertyChains as $propChain => $tmp) {
        $pChain = explode(' ', $propChain);
        $dtProp = NULL;
        if ($tmp === NULL) {
          // last property is a datatype property
          $dtProp = array_pop($pChain);
        }
#        dpm($dtProp, "yay!");

        $res = $graph->resources();
#        dpm(serialize(array_keys($res)), "res?");

        if(!in_array($uri, array_keys($res))) {
          $newuri = str_replace("http://", "https://", $uri);
          $resources = array($newuri => $newuri);
        } else {
          $resources = array($uri => $uri);
        }

        foreach ($pChain as $prop) {
          $newResources = array();
          foreach ($resources as $resource) {
#            dpm($graph->properties($resource), "props");
#            dpm($graph->allResources($resource, $prop), "Getting Resource $resource for prop $prop");
            foreach ($graph->allResources($resource, $prop) as $r) {
#              dpm($r, "er");
#              if(!empty($r->getUri())
              $newResources[$r->getUri()] = $r;
            }
          }
#          dpm($resources, "old");
#          dpm($newResources, "new");
          $resources = $newResources;
        }
        if ($dtProp) {
#          dpm($resources, "my res?");
          foreach ($resources as $resource) {
#            dpm($graph, "dtprop!");

            foreach ($graph->all($resource, $dtProp) as $thing) {
#              dpm($thing->getDatatype(), "thing");
#              dpm($dtProp, "dtprop!");

              if ($thing instanceof EasyRdf_Literal) {
                $lang = $thing->getLang();
                // fetch data in all languages used in this instance
                if (in_array($lang, $available_languages)) {
                  $data[$concept][$propChain][$langcode][] = $thing->getValue(). " (" . $lang . ")";
                }
                // special case "gvp:parentString" comes without language tag!
                elseif (is_null($lang)) {
                  $data[$concept][$propChain][$langcode][] = $thing->getValue();
                }
              }
            }
          }
        }
      }
    }

    $cache->set($id, $data);
#    dpm($data, "data");
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function checkUriExists($uri) {
    return !empty($this->fetchData($uri));
  }

  /**
   * {@inheritdoc}
   */
  public function createEntity($entity) {
    return;
  }


  public function getBundleIdsForEntityId($id) {
    $uri = $this->getUriForDrupalId($id);
    $data = $this->fetchData($uri);

    $pbs = $this->getPbsForThis();
    $bundle_ids = array();
    foreach($pbs as $key => $pb) {
      $groups = $pb->getMainGroups();
      foreach ($groups as $group) {
        $path = $group->getPathArray();
#dpm(array($path,$group, $pb->getPbPath($group->getID())),'bundlep');
        if (isset($data[$path[0]])) {
          $bid = $pb->getPbPath($group->getID())['bundle'];
#dpm(array($bundle_ids,$bid),'bundlesi');
          $bundle_ids[] = $bid;
        }
      }
    }
    return $bundle_ids;
  }

  /**
  * Helper function for database lookup
  */
  public function getUriForDrupalId($id, $create = FALSE) {
    // simple database lookup for EID
    if (is_int($id)) {
      $database = \Drupal::database();
      $query = $database->query("SELECT uri FROM {" . WisskiAdapterGettyInterface::DATABASE_TABLE . "} WHERE eid = :entity_id",
         [':entity_id' => $id,]);
      $uri = $query->fetchField();
      return $uri;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadFieldValues(array $entity_ids = NULL, array $field_ids = NULL, $bundle = NULL,$language = LanguageInterface::LANGCODE_DEFAULT) {

    if (!$entity_ids) {
      // TODO: get all entities
      $entity_ids = array(
        "http://vocab.getty.edu/aat/300015637"
      );
    }

    $out = array();

    foreach ($entity_ids as $eid) {
      foreach($field_ids as $fkey => $fieldid) {
        $got = $this->loadPropertyValuesForField($fieldid, array(), $entity_ids, $bundleid_in);

        if (empty($out)) {
          $out = $got;
        } else {
          foreach($got as $eid => $value) {
            if(empty($out[$eid])) {
              $out[$eid] = $got[$eid];
            } else {
              $out[$eid] = array_merge($out[$eid], $got[$eid]);
            }
          }
        }
      }
    }
    return $out;
  }

  /**
   * {@inheritdoc}
   */
  public function loadPropertyValuesForField($field_id, array $property_ids, array $entity_ids = NULL, $bundleid_in = NULL) {
#dpm(func_get_args(), 'lpvff');

    $main_property = FieldStorageConfig::loadByName('wisski_individual', $field_id);
    if(!empty($main_property)) {
      $main_property = $main_property->getMainPropertyName();
    }

    if(!empty($field_id) && empty($bundleid_in)) {
      $this->messenger()->addError("Es wurde $field_id angefragt und bundle ist aber leer.");
#      dpm(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
      return;
    }

    $pbs = array($this->getPbForThis());
    $paths = array();
    foreach($pbs as $key => $pb) {
      if (!$pb) continue;
      $field = $pb->getPbEntriesForFid($field_id);
#dpm(array($key,$field),'öäü');
      if (is_array($field) && !empty($field['id'])) {
        $paths[] = WisskiPathEntity::load($field["id"]);
      }
    }

    $out = array();

    foreach ($entity_ids as $eid) {

      if($field_id == "eid") {
        $out[$eid][$field_id] = array($eid);
      } elseif($field_id == "name") {
        // tempo hack
        $out[$eid][$field_id] = array($eid);
        continue;
      } elseif ($field_id == "bundle") {

      // Bundle is a special case.
      // If we are asked for a bundle, we first look in the pb cache for the bundle
      // because it could have been set by
      // measures like navigate or something - so the entity is always displayed in
      // a correct manor.
      // If this is not set we just select the first bundle that might be appropriate.
      // We select this with the first field that is there. @TODO:
      // There might be a better solution to this.
      // e.g. knowing what bundle was used for this id etc...
      // however this would need more tables with mappings that will be slow in case
      // of a lot of data...

        if(!empty($bundleid_in)) {
          $out[$eid]['bundle'] = array($bundleid_in);
          continue;
        } else {
          // if there is none return NULL
          $out[$eid]['bundle'] = NULL;
          continue;
        }
      } else {

        if (empty($paths)) {
#          $out[$eid][$field_id] = NULL;
        } else {

          // get current language
          $language =  \Drupal::service('language_manager')->getCurrentLanguage()->getId();

          foreach ($paths as $key => $path) {
            $values = $this->pathToReturnValue($path, $pbs[$key], $eid, 0, $main_property);
            if (!empty($values)) {
              foreach ($values as $lang => $val) {
                foreach ($val as $v) {
                  $out[$eid][$field_id][$language][] = $v;
                }
              }
            }
          }
        }
      }
    }
    return $out;
  }

  public function pathToReturnValue($path, $pb, $eid = NULL, $position = 0, $main_property = NULL) {
    #dpm($path->getName(), 'spam');
    $field_id = $pb->getPbPath($path->getID())["field"];

    $uri = $this->getUriForDrupalId($eid);
    $data = $this->fetchData($uri);
    if (!$data) {
      return [];
    }
    $path_array = $path->getPathArray();
    $path_array[] = $path->getDatatypeProperty();
    $data_walk = $data;
#    dpm($data_walk, "data");
#    dpm($path_array, "pa");
    do {
      $step = array_shift($path_array);
      if (isset($data_walk[$step])) {
        $data_walk = $data_walk[$step];
      } else {
        // this is oversimplified in case there is another path in question but this
        // one had no data. E.g. a preferred name exists, but no variant name and
        // the variant name is questioned. Then it will resolve most of the array
        // up to the property and then stop here.
        //
        // in this case nothing should stay in $data_walk because
        // the foreach below would generate empty data if there is something
        // left.
        // By Mark: I don't know if this really is what should be here, martin
        // @Martin: Pls check :)
        $data_walk = array();
        continue; // go to the next path
      }
    } while (!empty($path_array));
    // now data_walk contains only the values
    $out = array();
#    dpm($data_walk, "walk");
#    return $out;
    foreach ($data_walk as $lang => $arr) {
      foreach ($arr as $value) {
        if (empty($main_property)) {
          $out[$lang][] = $value;
        } else {
          $out[$lang][] = array($main_property => $value);
        }
      }
    }
    return $out;
  }

  /**
   * {@inheritdoc}
   */
  public function getPathAlternatives($history = [], $future = []) {
#    dpm($history);
    if (empty($history)) {
      $keys = array_keys($this->possibleSteps);
      return array_combine($keys, $keys);
    } else {
#      dpm($history, "hist");
      $steps = $this->possibleSteps;

#      dpm($steps, "keys");
      // go through the history deeper and deeper!
      foreach($history as $hist) {
#        $keys = array_keys($this->possibleSteps);

        // if this is not set, we can not go in there.
        if(!isset($steps[$hist])) {
          return array();
        } else {
          $steps = $steps[$hist];
        }
      }

      // see if there is something
      $keys = array_keys($steps);

      if(!empty($keys))
        return array_combine($keys, $keys);

      return array();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPrimitiveMapping($step) {
    $keys = array_keys($this->possibleSteps[$step]);
    return array_combine($keys, $keys);
  }

  /**
   * {@inheritdoc}
   */
  public function getStepInfo($step, $history = [], $future = []) {
    return array($step, '');
  }

  public function getQueryObject(EntityTypeInterface $entity_type, $condition, array $namespaces) {
    return new Query($entity_type, $condition, $namespaces);
  }

  public function providesDatatypeProperty() {
    return TRUE;
  }


}
