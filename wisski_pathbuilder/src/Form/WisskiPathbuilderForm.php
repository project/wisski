<?php

namespace Drupal\wisski_pathbuilder\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\FileRepositoryInterface;
use Drupal\wisski_pathbuilder\PathbuilderManager;
use Drupal\wisski_pathbuilder\Entity\WisskiPathbuilderEntity as Pathbuilder;
use Drupal\wisski_pathbuilder\Entity\WisskiPathEntity;
use Drupal\wisski_salz\Entity\Adapter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WisskiPathbuilderForm.
 *
 * Fom class for adding/editing WisskiPathbuilder config entities.
 */
class WisskiPathbuilderForm extends EntityForm {

  public $with_solr;

  /**
   * The FileRepositoryInterface service instance.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected FileRepositoryInterface $file;

  /**
   * The PathbuilderManager service instance.
   *
   * @var \Drupal\wisski_pathbuilder\PathbuilderManager
   */
  protected PathbuilderManager $pathbuilderManager;

  /**
   * Create service container.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The class container.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file.repository'),
      $container->get('wisski_pathbuilder.manager')
    );
  }

  /**
   * Constructs form variables.
   *
   * @param \Drupal\file\FileRepositoryInterface $file
   *   Performs file system operations and updates database records accordingly.
   */
  public function __construct(FileRepositoryInterface $file, PathbuilderManager $pathbuilderManager) {
    $this->file = $file;
    $this->pathbuilderManager = $pathbuilderManager;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // What entity do we work on?
    $pathbuilder = $this->entity;

    // Is solr enabled?
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('search_api_solr')) {
      $this->with_solr = TRUE;
    }

    // dpm($pathbuilder->getEntityType());
    // Change page title for the edit operation.
    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit Pathbuilder: @id', ['@id' => $pathbuilder->id()]);
    }

    // Only show this in create mode.
    if ($this->operation == 'add') {
      $form['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#default_value' => $pathbuilder->getName(),
        '#description' => $this->t("Name of the Pathbuilder-Tree."),
        '#required' => TRUE,
      ];

      // We need an id.
      $form['id'] = [
        '#type' => 'machine_name',
        '#default_value' => $pathbuilder->id(),
        '#disabled' => !$pathbuilder->isNew(),
        '#machine_name' => [
          'source' => ['name'],
          'exists' => '\Drupal\wisski_pathbuilder\Entity\WisskiPathbuilderEntity::load',
        ],
      ];
    }

    // Load all adapters.
    $adapters = Adapter::loadMultiple();

    $adapterlist = [];

    // Generate a list of all adapters.
    foreach ($adapters as $adapter) {
      // drupal_set_message(serialize($adapters));
      $adapterlist[$adapter->id()] = $adapter->label();
    }

    $ns = NULL;
    if (!empty($pathbuilder->getAdapterId())) {
      $adapter = Adapter::load($pathbuilder->getAdapterId());
      $engine = $adapter->getEngine();
      $ns = $engine->getNamespaces();
    }

    // If we are in edit mode, the options are below so the table
    // is set more directly at the top. Furthermore in the create mode
    // the table is unnecessary.
    if ($this->operation == 'edit') {
      if ($this->with_solr) {
        $header = [
          $this->t("Title"),
          $this->t("Path"),
          $this->t("Solr"),
          ['data' => $this->t("Enabled"), 'class' => ['checkbox']],
          $this->t('Field&nbsp;Type'),
          $this->t('Cardinality'),
          "Weight",
          ['data' => $this->t('Operations'), 'colspan' => 11],
        ];
      }
      else {
        $header = [
          $this->t("Title"),
          $this->t("Path"),
          ['data' => $this->t("Enabled"), 'class' => ['checkbox']],
          $this->t('Field&nbsp;Type'),
          $this->t('Cardinality'),
          "Weight",
          ['data' => $this->t('Operations'), 'colspan' => 11],
        ];
      }

      $form['pathbuilder_table'] = [
        '#type' => 'table',
      // '#theme' => 'table__menu_overview',
        '#header' => $header,
      // '#rows' => $rows,
        '#attributes' => [
          'id' => 'wisski_pathbuilder_' . $pathbuilder->id(),
        ],

        '#attached' => [
          'library' => [
            'wisski_pathbuilder/wisski_pathbuilder_collapse',
          ],
        ],

        '#tabledrag' => [

          [
            'action' => 'match',
            'relationship' => 'parent',
            'group' => 'menu-parent',
            'subgroup' => 'menu-parent',
            'source' => 'menu-id',
            'hidden' => TRUE,
            'limit' => 9,
          ],
          [
            'action' => 'order',
            'relationship' => 'sibling',
            'group' => 'menu-weight',
          ],
        ],

      ];

      $pathforms = [];

      // Get all paths belonging to the respective pathbuilder.
      foreach ($pathbuilder->getPathTree() as $grouparray) {

        $pathforms = array_merge($pathforms, $this->recursive_render_tree($grouparray, 0, 0, 0, $ns));
      }

      $pbpaths = $pathbuilder->getPbPaths();

      // Iterate through all the pathforms and bring the forms in a tree together.
      foreach ($pathforms as $pathform) {

        $path = $pathform['#item'];

        if (empty($path)) {
          $this->messenger()->addError("There is an empty Path in " . serialize($pathform));
          continue;
        }

        if (!(empty($path->getDatatypeProperty()) || $path->getDatatypeProperty() == "empty") && ($pbpaths[$path->id()]['fieldtype'] == "entity_reference" || $path->isGroup())) {
          $this->messenger()->addError("Danger Zone: Path '" . $path->label() . "' has field type 'entity reference' but uses " . $path->getDatatypeProperty() . " as datatype property. Please remove the datatype property.");
        }

        $form['pathbuilder_table'][$path->id()]['#item'] = $pathform['#item'];

        // TableDrag: Mark the table row as draggable.
        $form['pathbuilder_table'][$path->id()]['#attributes'] = $pathform['#attributes'];
        $form['pathbuilder_table'][$path->id()]['#attributes']['class'][] = 'draggable';

        // TableDrag: Sort the table row according to its existing/configured weight.
        $form['pathbuilder_table'][$path->id()]['#weight'] = $pbpaths[$path->id()]['weight'];

        // Add special classes to be used for tabledrag.js.
        $pathform['parent']['#attributes']['class'] = ['menu-parent'];
        $pathform['weight']['#attributes']['class'] = ['menu-weight'];
        $pathform['id']['#attributes']['class'] = ['menu-id'];

        $form['pathbuilder_table'][$path->id()]['title'] = [
          [
            '#theme' => 'indentation',
            '#size' => $pathform['#item']->depth,
          ],
          $pathform['title'],
        ];

        // $form['pathbuilder_table'][$path->id()]['path'] = array('#type' => 'label', '#title' => 'Mu -> ha -> ha');
        $form['pathbuilder_table'][$path->id()]['path'] = $pathform['path'];
        if ($this->with_solr) {
          $form['pathbuilder_table'][$path->id()]['solr'] = $pathform['solr'];
        }
        $form['pathbuilder_table'][$path->id()]['enabled'] = $pathform['enabled'];
        $form['pathbuilder_table'][$path->id()]['enabled']['#wrapper_attributes']['class'] = ['checkbox', 'menu-enabled'];

        $form['pathbuilder_table'][$path->id()]['field_type_informative'] = $pathform['field_type_informative'];
        $form['pathbuilder_table'][$path->id()]['cardinality'] = $pathform['cardinality'];

        $form['pathbuilder_table'][$path->id()]['weight'] = $pathform['weight'];

        // Get bundle id, sometimes fields do not have bundle value.
        $bundle_id = $pbpaths[$path->id()]['bundle'];
        $bundle_id_from_parent = $pbpaths[$pbpaths[$path->id()]['parent']]['bundle'] ?? NULL;
        if (empty($bundle_id)) {
          $bundle_id = $bundle_id_from_parent;
        }

        $field_id = $pbpaths[$path->id()]['field'];

        // An array of links that can be selected in the dropdown operations list
        // edit path.
        $links = [];
        $links['edit'] = [
          'title' => $this->t('Edit'),
        // 'url' => $path->urlInfo('edit-form', array('wisski_pathbuilder'=>$pathbuilder->getID())),
          'url' => Url::fromRoute('entity.wisski_path.edit_form')
            ->setRouteParameters(['wisski_pathbuilder' => $pathbuilder->id(), 'wisski_path' => $path->id()]),
        ];

        // Edit field config in pathbuilder environment.
        $links['fieldconfig'] = [
          'title' => $this->t('Configure Field'),
        // 'url' => $path->urlInfo('edit-form', array('wisski_pathbuilder'=>$pathbuilder->id())),
          'url' => Url::fromRoute('entity.wisski_pathbuilder.configure_field_form')
            ->setRouteParameters(['wisski_pathbuilder' => $pathbuilder->id(), 'wisski_path' => $path->id()]),
        ];

        /*
        if(!empty($pbpaths[$path->id()]) && !empty($bundle_id)) {
        $links['formedit'] = array(
        'title' => $this->t('Manage Form Display for Bundle'),
        # 'url' => $path->urlInfo('edit-form', array('wisski_pathbuilder'=>$pathbuilder->id())),
        'url' => Url::fromRoute('entity.entity_form_display.wisski_individual.default')
        ->setRouteParameters(array('wisski_bundle' => $bundle_id)),
        );
         */

        /*
        $links['displayedit'] = array(
        'title' => $this->t('Manage Display for Bundle'),
        # 'url' => $path->urlInfo('edit-form', array('wisski_pathbuilder'=>$pathbuilder->id())),
        'url' => Url::fromRoute('entity.entity_view_display.wisski_individual.default')
        ->setRouteParameters(array('wisski_bundle' => $bundle_id)),
        );
        }
         */

        $links['delete_local'] = [
          'title' => $this->t('Delete path only from this pathbuilder'),
          'url' => Url::fromRoute('entity.wisski_path.delete_local_form')
            ->setRouteParameters(['wisski_pathbuilder' => $pathbuilder->id(), 'wisski_path' => $path->id()]),
        ];

        $links['delete'] = [
          'title' => $this->t('Delete path completely'),
          'url' => Url::fromRoute('entity.wisski_path.delete_form')
            ->setRouteParameters(['wisski_pathbuilder' => $pathbuilder->id(), 'wisski_path' => $path->id()]),
        ];

        $links['duplicate'] = [
          'title' => $this->t('Duplicate'),
          'url' => Url::fromRoute('entity.wisski_path.duplicate_form')
            ->setRouteParameters(['wisski_pathbuilder' => $pathbuilder->id(), 'wisski_path' => $path->id()]),
        ];

        if (!empty($bundle_id)) {
          // Go to bundle structure.
          if ($path->isGroup()) {
            $links['bundleedit'] = [
              'title' => $this->t('Shortcut to Structure'),
            // 'url' => $path->urlInfo('edit-form', array('wisski_pathbuilder'=>$pathbuilder->id())),
              'url' => Url::fromRoute('entity.wisski_bundle.edit_form')
                ->setRouteParameters(['wisski_bundle' => $bundle_id]),
            ];

            // Go to field structure.
          }
          else {
            $links['fieldsedit'] = [
              'title' => $this->t('Shortcut to Structure'),
            // 'url' => $path->urlInfo('edit-form', array('wisski_pathbuilder'=>$pathbuilder->id())),
              'url' => Url::fromRoute('entity.field_config.wisski_individual_field_edit_form')
                ->setRouteParameters(['wisski_bundle' => $bundle_id, 'field_config' => 'wisski_individual.' . $bundle_id . '.' . $field_id]),
            ];
          }
        }

        // Operations (dropbutton) column.
        // $operations = parent::getDefaultOperations($pathbuilder);
        $operations = [
          '#type' => 'operations',
          '#links' => $links,
        ];

        $form['pathbuilder_table'][$path->id()]['operations'] = $operations;

        $form['pathbuilder_table'][$path->id()]['id'] = $pathform['id'];

        $form['pathbuilder_table'][$path->id()]['parent'] = $pathform['parent'];

        // If the parent is not part of this pathbuilder, the path should be attached to top.
        if (empty($form['pathbuilder_table'][$pathform['parent']['#value']])) {
          $form['pathbuilder_table'][$path->id()]['parent']['#value'] = 0;
        }

        if (!empty($pathform['bundle'])) {
          $form['pathbuilder_table'][$path->id()]['bundle'] = $pathform['bundle'];
        }
        if (!empty($pathform['field'])) {
          $form['pathbuilder_table'][$path->id()]['field'] = $pathform['field'];
        }
        if (!empty($pathform['fieldtype'])) {
          $form['pathbuilder_table'][$path->id()]['fieldtype'] = $pathform['fieldtype'];
        }
        if (!empty($pathform['displaywidget'])) {
          $form['pathbuilder_table'][$path->id()]['displaywidget'] = $pathform['displaywidget'];
        }
        if (!empty($pathform['formatterwidget'])) {
          $form['pathbuilder_table'][$path->id()]['formatterwidget'] = $pathform['formatterwidget'];
        }
        // drupal_set_message(serialize($form['pathbuilder_table'][$path->id()]));
        // dpm($form['pathbuilder_table'][$path->id()],'Path '.$path->id());
      }
    }

    // Additional information stored in a field set below.
    $form['additional'] = [
      '#type' => 'fieldset',
      '#tree' => FALSE,
      '#title' => $this->t('Additional Settings'),
    ];

    // Only show this in edit mode.
    if ($this->operation == 'edit') {
      $form['additional']['name'] = [
        '#type' => 'textfield',
        '#maxlength' => 2048,
        '#title' => $this->t('Name'),
        '#default_value' => $pathbuilder->getName(),
        '#description' => $this->t("Name of the Pathbuilder-Tree."),
        '#required' => TRUE,
      ];

      // We need an id.
      $form['additional']['id'] = [
        '#type' => 'machine_name',
        '#default_value' => $pathbuilder->id(),
        '#disabled' => !$pathbuilder->isNew(),
        '#machine_name' => [
          'source' => ['additional', 'name'],
          'exists' => '\Drupal\wisski_pathbuilder\Entity\WisskiPathbuilderEntity::load',
        ],
      ];

      if ($this->with_solr) {
        $form['additional']['with_solr'] = [
          '#type' => 'button',
          '#value' => $this->t('Show Solr Paths'),
          '#attributes' => [
            'onclick' => 'return false;',
          ],
          '#attached' => [
            'library' => [
              'wisski_pathbuilder/wisski_pathbuilder_solr',
            ],
          ],
        ];
      }
      // $form['additional']['with_solr'] = array(
      // '#type' => 'checkbox',
      // '#title' => $this->t('Show Solr Paths'),
      // '#default_value' => $this->with_solr,
      // '#description' => $this->t("Show the solr paths."),
      // );
    }

    // Change the adapter this pb belongs to?
    $form['additional']['adapter'] = [
      '#type' => 'select',
      '#description' => $this->t('Which adapter does this Pathbuilder belong to?'),
      '#default_value' => $pathbuilder->getAdapterId(),
    // array(0 => "Pathbuilder"),.
      '#options' => $adapterlist,
    ];
    // This is obsolete.
    /*
    // what is the create mode?
    $form['additional']['create_mode'] = array(
    '#type' => 'select',
    '#description' => $this->t('What should be generated on save?'),
    '#default_value' => empty($pathbuilder->getCreateMode()) ? 'wisski_bundle' : $pathbuilder->getCreateMode(),
    '#options' => array('field_collection' => 'field_collection', 'wisski_bundle' => 'wisski_bundle'),
    );
     */
    $form['import'] = [
      '#type' => 'fieldset',
      '#tree' => FALSE,
      '#title' => $this->t('Import Templates'),
    ];

    $form['import']['import'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pathbuilder Definition Import'),
      '#description' => $this->t('Path to a pathbuilder definition file.'),
      '#maxlength' => 2048,
    // '#default_value' => $pathbuilder->getCreateMode(),
    // '#options' => array('field_collection' => 'field_collection', 'wisski_bundle' => 'wisski_bundle'),
    ];

    $field_options = [
      'keep' => $this->t('Keep settings from import file'),
      Pathbuilder::CONNECT_NO_FIELD => $this->t('Do not create fields and bundles'),
      Pathbuilder::GENERATE_NEW_FIELD => $this->t('Create new fields and bundles'),
    ];

    $form['import']['import_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Set default mode to'),
      '#description' => $this->t('<div>What should the fields and groups mode be set to?<br><ul><li><strong>Keep settings</strong>: Paths and their field types, cardinality, activation status, and ids etc. are imported as in the import file.</li><li><strong>Create</strong>: Paths are imported and enabled, but field types, and path and field id generation are set to default behavior.</li><li><strong>Do not create</strong>: Paths are imported, but not enabled and their field types, and the path and field id generation are set to default.</li></ul></div>'),
      '#options' => $field_options,
      '#default_value' => 'keep',
    ];

    $form['import']['importbutton'] = [
      '#type' => 'submit',
      '#value' => 'Import',
      '#submit' => ['::import'],
    // '#description' => $this->t('Path to a pathbuilder definition file.'),
    // '#default_value' => $pathbuilder->getCreateMode(),
    // '#options' => array('field_collection' => 'field_collection', 'wisski_bundle' => 'wisski_bundle'),
    ];

    $form['export'] = [
      '#type' => 'fieldset',
      '#tree' => FALSE,
      '#title' => $this->t('Export Templates'),
    ];

    $export_path = 'public://wisski_pathbuilder/export/';

    \Drupal::service('file_system')->prepareDirectory($export_path, FileSystemInterface::CREATE_DIRECTORY);

    $files = \Drupal::service('file_system')->scanDirectory($export_path, '/.*/');

    ksort($files);

    $items = [];

    foreach ($files as $file) {
      // $form['export']['export'][] = array('#type' => 'link', '#title' => $file->filename, '#url' => Url::fromUri(file_create_url($file->uri)));
      $items[] = ['#type' => 'link', '#title' => $file->filename, '#url' => \Drupal::service('file_url_generator')->generate($file->uri)];
    }

    $form['export']['export'] = [
      '#theme' => 'item_list',
    // '#title' => 'Existing exports',
      '#items' => $items,
      '#type' => 'ul',
      '#attributes' => ['class' => 'pb_export'],
    ];

    $form['export']['exportbutton'] = [
      '#type' => 'submit',
      '#value' => 'Create Exportfile',
      '#submit' => ['::export'],
    // '#description' => $this->t('Path to a pathbuilder definition file.'),
    // '#default_value' => $pathbuilder->getCreateMode(),
    // '#options' => array('field_collection' => 'field_collection', 'wisski_bundle' => 'wisski_bundle'),
    ];

    $form['#attached'] = [
      'library' => [
        'wisski_pathbuilder/wisski_pathbuilder',
      ],
    ];

    // dpm($form);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {

    $element = parent::actions($form, $form_state);
    $element['#type'] = '#dropbutton';
    $element['#attributes'] = ['class' => ['wisski-pathbuilder__submit-region']];

    // Only add this to "normal" ones...
    $entityType = $this->entity->getType();
    $entityName = $this->entity->getName();
    $entityId = $this->entity->id();

    if ($entityType != "linkblock" && !is_null($entityId)) {
      if (!empty($entityName)) {
        if (strpos($entityName, "(Linkblock)") === FALSE && $entityName != "WissKI Linkblock PB") {
          $element['generate'] = [
            '#type' => 'submit',
            '#value' => $this->t('Save and generate bundles and fields'),
            '#submit' => ['::submitForm', '::save_and_generate_forms'],
            '#weight' => -10,
            '#dropbutton' => 'save',
            '#attributes' => [
              'class' => ['wisski-pathbuilder__submit-button'],
            ],
          ];
        }
      }
    }

    $element['submit']['#value'] = $this->t('Save without form generation');
    $element['submit']['#dropbutton'] = 'save';
    $element['submit']['#attributes'] = ['class' => ['wisski-pathbuilder__submit-button']];
    return $element;
  }

  /**
   * Exports pathbuilder structure.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @throws \Exception
   */
  public function export(array &$form, FormStateInterface $form_state, ?array $paths = NULL) {
    $xml = $this->entity->toXML();
    $export_path = 'public://wisski_pathbuilder/export/' . $this->entity->id() . date('_Ymd\THis');
    $this->file->writeData($xml, $export_path, FileSystemInterface::EXISTS_RENAME);
  }


  /**
   * Recursive function that gets an array of the parents of a given
   * group id.
   *
   * @param array $import
   *   All paths of the pathbuilder as a key => value array
   * @param string $to_look_for
   *   The id to look for as a starting point.
   *
   */
  private function getParents(array $import, string $to_look_for) {
    // initialize array
    $ret = array();

    if(!isset($import[$to_look_for])) {
      //dpm($import, "Could not find $to_look_for");
      $this->messenger()->addStatus("Path with id " . $to_look_for . " was not found as a parent - skipping all paths that have this.");
      return $ret;
    }


    // get the parent of the id to look for
    $parent = ((string)$import[$to_look_for]->group_id);

    // if it has no parent we don't return something here, so it
    // should be empty array.
    // if we have something here we recursively fetch the key => value array
    // for the parent.
    if($parent != "0") {
      // only do this if there really is such a parent
      if(isset($import[$parent]))
        $ret = $this->getParents($import, $parent);

      // add it to the array
      $ret[$parent] = $import[$to_look_for];

    }
    return $ret;
  }


  /**
   *
   */
  public function import(array &$form, FormStateInterface $form_state) {

    $importfile = $form_state->getValue('import');

    $importmode = $form_state->getValue('import_mode');

    // dpm($importfile, "importfile!!");.
    $xmldoc = new \SimpleXMLElement($importfile, 0, TRUE);

#    dpm($xmldoc, "doc!");
    $pb = $this->entity;

    $groups = array();
    $paths = array();

    $all = array();
    // first generate a mapping of all paths/groups to their ids for faster access
    foreach ($xmldoc->path as $pot_group) {
      $all[((string)$pot_group->id)] = $pot_group;
    }

    // iterate all paths
    foreach ($xmldoc->path as $pot_group) {
      // find the groups
      if(((int)$pot_group->is_group) === 1) {
        // if the group has a parent - we have to make sure
        // the parent is above the group itself.
        // so we preload it.
        if(((string)$pot_group->group_id) != "0") {
          // get an array of all parent group ids
          $ret = $this->getParents($all, ((string)$pot_group->group_id));

          // and add them to the groups array
          foreach($ret as $key => $one_ret) {
            if(!isset($groups[$key])) {
              $groups[$key] = $one_ret;
            }
          }
        }

        // if the group is not there already, add it.
        if(!isset($groups[((string)$pot_group->id)]))
          $groups[((string)$pot_group->id)] = $pot_group;
      }
      else {
        // if it is a path, add it to the paths array.
        $paths[] = $pot_group;
      }
    }

    // add the paths back at the end of the array
    // this makes sure that the groups are interpreted first
    foreach($paths as $path) {
      $groups[] = $path;
    }
    
    

    foreach ($groups as $path) {
    
      // here we want to make sure that everything that is not included
      // in the import file is preset with a useful default value
      // and if there is no default value somebody probably
      // should handle something here. 
      
      if(property_exists($path, "group_id") && isset($path->group_id)) {         
        $parentid = html_entity_decode((string) $path->group_id);
      } else // if we don't have a parent id, add it to the top level.
        $parentid = 0;

      // if($parentid != 0)
      // $parentid = wisski_pathbuilder_check_parent($parentid, $xmldoc);.
      if(property_exists($path, "uuid") && isset($path->uuid)) 
        $uuid = html_entity_decode((string) $path->uuid);
      else {
        $uuid_service = \Drupal::service('uuid');
        $uuid = $uuid_service->generate();  
      }
        
        
      if(!property_exists($path, "id") || !isset($path->id)) {
        $this->messenger()->addStatus("I found a path that had no id in the Import file. This is evil. I skipped this path, please have a look at the provided import file and make sure that every <path> element has an <id> element in it.");
        continue;
      }
        
      // if(empty($uuid))
      // Check if path already exists.
      $path_in_wisski = WisskiPathEntity::load((string) $path->id);

      // It exists, skip this...
      if (!empty($path_in_wisski)) {
        $this->messenger()->addStatus("Path with id " . $uuid . " was already existing - skipping.");

        $pb->addPathToPathTree($path_in_wisski->id(), $parentid, $path_in_wisski->isGroup());

        // continue;.
      }
      // Normal case - import the path!
      else {

        $path_array = [];
        $count = 0;
        
        if(!property_exists($path, "path_array") || !isset($path->path_array)) {
          $this->messenger()->addStatus("I found a path that had no path array in the Import file. This is evil. I skipped this path, please have a look at the provided import file and make sure that every <path> element has an <path_array> element in it.");
          continue;
        }
        
        foreach ($path->path_array->children() as $n) {
          $path_array[$count] = html_entity_decode((string) $n);
          $count++;
        }
        
        $default_values_for_path = [
          // id should be present, because otherwise we would have exited above.
          'name' => 'Path ' . html_entity_decode((string) $path->id), // default path name in case there is no name attribute present.
          // path_array should be present, because otherwise we would have exited above.
          'datatype_property' => '',
          'short_name' => '',
          'length' => $count,
          'disamb' => 0, // this means no disamb
          'description' => (property_exists($path, "name") && isset($path->name)) ? html_entity_decode((string) $path->name) : ('Path ' . html_entity_decode((string) $path->id)), // if we have no name then take the default one.
          // type is set below.
        ];

        // It does not exist, create one!
        $pathdata = [
          'id' => html_entity_decode((string) $path->id),
          'name' => (property_exists($path, "name") && isset($path->name)) ? html_entity_decode((string) $path->name) : $default_values_for_path['name'],
          'path_array' => $path_array,
          'datatype_property' => (property_exists($path, "datatype_property") && isset($path->datatype_property)) ? html_entity_decode((string) $path->datatype_property) : $default_values_for_path['datatype_property'],
          'short_name' => (property_exists($path, "short_name") && isset($path->short_name)) ? html_entity_decode((string) $path->short_name) : $default_values_for_path['short_name'],
          'length' => (property_exists($path, "length") && isset($path->length)) ? html_entity_decode((string) $path->length) : $default_values_for_path['length'],
          'disamb' => (property_exists($path, "disamb") && isset($path->disamb)) ? html_entity_decode((string) $path->disamb) : $default_values_for_path['disamb'],
          'description' => (property_exists($path, "description") && isset($path->description)) ? html_entity_decode((string) $path->description) : $default_values_for_path['description'],
          'type' => (((int) $path->is_group) === 1) ? 'Group' : 'Path',
        // 'field' => Pathbuilder::GENERATE_NEW_FIELD,
        ];
        
#        dpm($pathdata, "yay, ?");
#        return;

        // In D8 we do no longer allow a path/group without a name.
        // we have to set it to a dummy value.
        if ($pathdata['name'] == '') {
          $pathdata['name'] = "_empty_";
          $this->messenger()->addWarning(t('Path with id @id (@uuid) has no name. Name has been set to "_empty_".', ['@id' => $pathdata['id'], '@uuid' => $uuid]));
        }

        $path_in_wisski = WisskiPathEntity::create($pathdata);

        $path_in_wisski->save();

        $pb->addPathToPathTree($path_in_wisski->id(), $parentid, $path_in_wisski->isGroup());
      }

      // Check enabled or disabled.
      $pbpaths = $pb->getPbPaths();

      $pbpaths[$path_in_wisski->id()]['enabled'] = (property_exists($path, "enabled") && isset($path->enabled)) ? html_entity_decode((string) $path->enabled) : '1';
      $pbpaths[$path_in_wisski->id()]['weight'] = (property_exists($path, "weight") && isset($path->weight)) ? html_entity_decode((string) $path->weight) : '1';
      $pbpaths[$path_in_wisski->id()]['cardinality'] = (property_exists($path, "cardinality") && isset($path->cardinality)) ? html_entity_decode((string) $path->cardinality) : '0';

      if (html_entity_decode((string) $importmode) != "keep") {
        if (((int) $path->is_group) === 1) {
          $pbpaths[$path_in_wisski->id()]['bundle'] = (property_exists($path, "bundle") && isset($path->bundle)) ? html_entity_decode((string) $importmode) : Pathbuilder::GENERATE_NEW_FIELD;
        }
        else {
          $pbpaths[$path_in_wisski->id()]['field'] = (property_exists($path, "field") && isset($path->field)) ? html_entity_decode((string) $importmode) : Pathbuilder::GENERATE_NEW_FIELD;
        }
      }
      else {
        $pbpaths[$path_in_wisski->id()]['bundle'] = (property_exists($path, "bundle") && isset($path->bundle)) ? html_entity_decode((string) $path->bundle) : Pathbuilder::GENERATE_NEW_FIELD;
        $pbpaths[$path_in_wisski->id()]['field'] = (property_exists($path, "field") && isset($path->field)) ? html_entity_decode((string) $path->field) : Pathbuilder::GENERATE_NEW_FIELD;

        if (property_exists($path, "fieldtype") && $path->fieldtype) {
          $pbpaths[$path_in_wisski->id()]['fieldtype'] = html_entity_decode((string) $path->fieldtype);
        }

        if (property_exists($path, "displaywidget") && $path->displaywidget) {
          $pbpaths[$path_in_wisski->id()]['displaywidget'] = html_entity_decode((string) $path->displaywidget);
        }

        if (property_exists($path, "formatterwidget") && $path->formatterwidget) {
          $pbpaths[$path_in_wisski->id()]['formatterwidget'] = html_entity_decode((string) $path->formatterwidget);
        }
      }
      $pb->setPbPaths($pbpaths);

    }

    $pb->save();

    // drupal_set_message(serialize($pb->getPbPaths()));
  }

  /**
   *
   */
  private function recursive_render_tree($grouparray, $parent = 0, $delta = 0, $depth = 0, $namespaces = NULL, $solr = "") {
    // dpm(microtime(), "1");
    // First we have to get any additional fields because we just got the tree-part
    // and not the real data-fields.
    $pbpath = $this->entity->getPbPath($grouparray['id']);

    // If we did not get something, stop.
    if (empty($pbpath)) {
      return [];
    }

    // if(empty($namespaces))
    // $namespaces =.
    // Merge it into the grouparray.
    $grouparray = array_merge($grouparray, $pbpath);

    if (!isset($grouparray['cardinality'])) {
      $grouparray['cardinality'] = -1;
    }

    // What to add to solr in this case?
    $group_solr = $grouparray['field'] ?? $grouparray['bundle'];

    if (empty($solr)) {
      $group_solr = "entity:wisski_individual/";
    }
    elseif ($solr == "entity:wisski_individual/") {
      // Special case if it is the first thingie.
      $group_solr = $group_solr;
    }
    else {
      $group_solr = ":entity:" . $group_solr;
    }

    $pathform[$grouparray['id']] = $this->pb_render_path($grouparray['id'], $grouparray['enabled'], $grouparray['weight'], $depth, $parent, $grouparray['bundle'], $grouparray['field'], $grouparray['fieldtype'], $grouparray['displaywidget'], $grouparray['formatterwidget'], $grouparray['cardinality'], $namespaces, $solr . $group_solr);

    if (is_null($pathform[$grouparray['id']])) {
      unset($pathform[$grouparray['id']]);
      return [];
    }

    $subforms = [];

    $children = [];
    $weights = [];

    foreach ($grouparray['children'] as $key => $child) {
      $pbp = $this->entity->getPbPath($key);
      // $solrs[$key] = isset($pbp['field']) ? $pbp['field'] : $pbp['group'];
      $weights[$key] = $pbp['weight'];
    }

    $children = $grouparray['children'];

    array_multisort($weights, $children);

    if (empty($pathform[$grouparray['id']]['#item'])) {
      return [];
    }

    $mypath = $pathform[$grouparray['id']]['#item']->getPathArray();

    // $origpf = $pathform;
    foreach ($children as $childpath) {
      $subform = $this->recursive_render_tree($childpath, $grouparray['id'], $delta, $depth + 1, $namespaces, $solr . $group_solr);

      // Check if the group is correct.
      foreach ($subform as $sub) {

        if (empty($sub['#item'])) {
          continue;
        }

        $subpath = $sub['#item']->getPathArray();

        // Calculate the diff between the subpath and the group.
        $diff = array_diff($subpath, $mypath);

        // And do it the primitive way.
        $difflength = count($subpath) - count($mypath);

        // If these differ there is something fishy!
        if (count($diff) > $difflength) {
          $subname = $sub['#item']->getName();
          $pathname = $pathform[$grouparray['id']]['#item']->getName();

          $this->messenger()->addError("Path " . $subname . " conflicts with definition of group " . $pathname . ". Please check.");
          $pathform[$grouparray['id']]['#attributes'] = ['style' => ['background-color: red']];
        }

      }
      // dpm($pathform, "before");
      // dpm($subform, "before2");.
      if (!empty($subform)) {
        $pathform = $pathform + $subform;
      }

      // dpm($pathform, "after");
      // return;.
    }
    // dpm(microtime(), "2");.
    return $pathform;

  }

  /**
   *
   */
  private function wisski_weight_sort($a, $b) {
    // dpm("I am alive!");.
    if (intval($a['weight']['#default_value']) == intval($b['weight']['#default_value'])) {
      return 0;
    }

    return (intval($a['weight']['#default_value']) < intval($b['weight']['#default_value'])) ? -1 : 1;
  }

  /**
   *
   */
  private function pb_render_path($pathid, $enabled, $weight, $depth, $parent, $bundle, $field, $fieldtype, $displaywidget, $formatterwidget, $cardinality, $namespaces, $solr) {
    $path = \Drupal::service('entity_type.manager')->getStorage('wisski_path')->load($pathid);

    if (is_null($path)) {
      return NULL;
    }

    $pathform = [];

    $item = [];

    // $item['#title'] = $path->getName();
    $path->depth = $depth;

    $pathform['#item'] = $path;

    $pathform['#attributes'] = $enabled ? ['class' => ['menu-enabled']] : ['class' => ['menu-disabled']];

    // $pathform['title'] = '<a href="/dev/contact" data-drupal-selector="edit-links-menu-plugin-idcontactsite-page-title-1" id="edit-links-menu-plugin-idcontactsite-page-title-1" class="menu-item__link">Contact</a>';
    // $path->name;
    $pathform['title'] = [
      '#type' => 'label',
      '#title' =>
      $path->getName(),
      '#attributes' => $path->isGroup() ? ['data-pathbuilder-group' => 'true', 'style' => 'font-weight: bold;'] : ['data-pathbuilder-group' => 'false', 'style' => 'font-weight: normal; font-style:italic;'],
    ];

    if (!$enabled) {
      $pathform['title']['#suffix'] = ' (' . $this->t('disabled') . ')';
    }

    /*
    $pathform['path'] = array(
    '#type' => 'item',
    '#markup' => $path->printPath($namespaces),
    );
     */

    $pathform['path'] = [
      '#markup' => $path->printPath($namespaces),
      '#allowed_tags' => ['span'],
    ];

    if (!$this->with_solr) {
      // $pathform['solr']['#type'] = 'hidden';
      // $pathform['solr']['#value'] = $solr;
    }
    else {
      $pathform['solr'] = [
        '#markup' => "<span class = 'wki-pb-solr'>" . $solr . "</span>",
        '#allowed_tags' => ['span'],
      ];
    }

    // If it is a group, mark it as such.
    if ($path->isGroup()) {
      $pathform['path']['#markup']  = 'Group [' . $pathform['path']['#markup'];
      $pathform['path']['#markup'] .= ']';
    }

    $pathform['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable @title path', ['@title' => $path->getName()]),
      '#title_display' => 'invisible',
      '#default_value' => $enabled,
    ];

    $field_type_label = $fieldtype ? \Drupal::service('plugin.manager.field.field_type')->getDefinition($fieldtype)['label'] : '';
    $pathform['field_type_informative'] = [
      '#type' => 'item',
      '#value' => $fieldtype,
      '#markup' => $field_type_label,
    ];

    $pathform['cardinality'] = [
      '#type' => 'item',
      '#markup' => WisskiPathbuilderConfigureFieldForm::cardinalityOptions()[$cardinality],
      '#value' => $cardinality,
      '#title' => $this->t('Field Cardinality'),
      '#title_display' => 'attribute',
    ];

    $pathform['weight'] = [
    // '#type' => 'weight',
      '#type' => 'textfield',
    // '#delta' => 100, # Do something more cute here $delta,
      '#default_value' => $weight,
      '#title' => $this->t('Weight for @title', ['@title' => $path->getName()]),
      '#title_display' => 'invisible',
    ];

    $pathform['id'] = [
    // '#type' => 'value',
      '#type' => 'hidden',
      '#value' => $path->id(),
    ];

    $pathform['parent'] = [
    // '#type' => 'value',
      '#type' => 'hidden',
      '#value' => $parent,
    ];

    // All this information is not absolutely necessary for the pb - so we skip it here.
    // if we don't do this max_input_vars and max_input_nesting overflows.
    /*
    $pathform['bundle'] = array(
    #      '#type' => 'value',
    '#type' => 'hidden',
    '#value' => $bundle,
    );

    $pathform['field'] = array(
    '#type' => 'value',
    #      '#type' => 'hidden',
    '#value' => $field,
    '#markup' => $field,
    );

    $pathform['fieldtype'] = array(
    #      '#type' => 'value',
    '#type' => 'hidden',
    '#value' => $fieldtype,
    );

    $pathform['displaywidget'] = array(
    #     '#type' => 'value',
    '#type' => 'hidden',
    '#value' => $displaywidget,
    );

    $pathform['formatterwidget'] = array(
    #      '#type' => 'value',
    '#type' => 'hidden',
    '#value' => $formatterwidget,
    );
     */
    return $pathform;
  }

  /**
   *
   */
  public function save_and_generate_forms(array $form, FormStateInterface $form_state) {
    // Get the pathbuilder.
    $pathbuilder = $this->entity;

    // Fetch the paths.
    $paths = $form_state->getValue('pathbuilder_table');

    $this->save($form, $form_state);

    $this->pathbuilderManager->saveAndGenerate($pathbuilder, $paths);

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    // Get the pathbuilder.
    $pathbuilder = $this->entity;

    // Fetch the paths.
    $paths = $form_state->getValue('pathbuilder_table');

    $pathtree = [];
    $map = [];

    // Regardless of what it is - we have to save it properly to the pbpaths.
    $pbpaths = $pathbuilder->getPbPaths();

    if (!empty($paths)) {
      foreach ($paths as $key => $path) {
        // $pathtree = array_merge($pathtree, $this->recursive_build_tree(array($key => $path)));
        // It has parents... we have to add it somewhere.
        if (!empty($path['parent'])) {
          $map[$path['parent']]['children'][$path['id']] = ['id' => $path['id'], 'children' => []];
          $map[$path['id']] = &$map[$path['parent']]['children'][$path['id']];
        }
        // It has no parent - so it is a main thing.
        else {
          $pathtree[$path['id']] = ['id' => $path['id'], 'children' => []];
          $map[$path['id']] = &$pathtree[$path['id']];
        }

        // mark: I don't know what dorian wanted to do here. If somebody can explain to me
        // it might be useful - as long as nobody can, revert.
        // $pbpaths[$path['id']] = \Drupal\wisski_core\WisskiHelper::array_merge_nonempty($pbpaths[$path['id']],$path); #array('id' => $path['id'], 'weight' => $path['weight'], 'enabled' => $path['enabled'], 'children' => array(), 'bundle' => $path['bundle'], 'field' => $path['field']);.
        // This works, but for performance reason we try to have nothing in the pathbuilder
        // overview what not absolutely has to be there.
        // $pbpaths[$path['id']] = $path;.
        // dpm($pbpaths[$path['id']], 'old');
        // dpm($path, 'new');
        // dpm(array_merge($pbpaths[$path['id']], $path), 'merged');.
        $pbpaths[$path['id']] = array_merge($pbpaths[$path['id']], $path);
      }
    }

    // Save the path.
    $pathbuilder->setPbPaths($pbpaths);

    // dpm(array('old' => $pathbuilder->getPathTree(),'new' => $pathtree, 'form paths' => $paths),'Path trees');
    // Save the tree.
    $pathbuilder->setPathTree($pathtree);

    // $pathbuilder->setWithSolr($form_state->getValue("with_solr"));
    $status = $pathbuilder->save();

    if ($status) {
      // Setting the success message.
      $this->messenger()->addStatus($this->t('Saved the pathbuilder: @id.', [
        '@id' => $pathbuilder->id(),
      ]));
    }
    else {
      $this->messenger()->addError($this->t('The Pathbuilder @id could not be saved.', [
        '@id' => $pathbuilder->id(),
      ]));
    }
    // dpm($pathbuilder, "Pb");
    // if(!is_null($pathbuilder->id()))
    $form_state->setRedirect('entity.wisski_pathbuilder.edit_form', ['wisski_pathbuilder' => $this->entity->id()]);
    // else
    // $form_state->setRedirect('entity.wisski_pathbuilder.collection');.
  }

}
