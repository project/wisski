<?php

namespace Drupal\wisski_pathbuilder\Drush\Commands;

use Drupal\wisski_pathbuilder\Entity\WisskiPathbuilderEntity;
use Drupal\wisski_pathbuilder\PathbuilderManager;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * A Drush commandfile.
 */
final class WisskiPathbuilderCommands extends DrushCommands {

  use AutowireTrait;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected SerializerInterface $serializer;

  /**
   * Constructs a WisskiPathbuilderCommands object.
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * List machine names of all pathbuilders.
   *
   * @command wisski-pathbuilder:list
   *
   * @usage drush wisski-pathbuilder:liste
   *   List all pathbuilders in the system..
   */
  public function listPathbuilders() {
    $pathbuilders = \Drupal::entityTypeManager()->getStorage("wisski_pathbuilder")->loadMultiple();
    foreach (array_keys($pathbuilders) as $machine_name) {
      $this->output()->writeln($machine_name);
    }
  }

  /**
   * Export pathbuilders. This exports the pathbuilders into the root web directory.
   *
   * @param array $machine_names
   *   Machine name of the pathbuilders to export.
   * @param array $options
   *   The options.
   *
   * @command wisski-pathbuilder:export
   *
   * @option all Export all pathbuilders. (optional, default=False).
   *
   * @usage drush wisski-pathbuilder:export gemaeldesammlung gemeinsame_masken
   *   Export two pathbuilders by ID.
   * @usage drush wisski-pathbuilder:export --all
   *   Export all pathbuilders.
   */
  public function exportPathbuilder(array $machine_names, $options = ['all' => FALSE]) {
    if (empty($machine_names) and !$options['all']) {
      $this->output()->writeln("No pathbuilder specified. If you wish to export all pathbuilders use the --all option");
      return;
    }

    if ($options['all']) {
      $machine_names = NULL;
    }

    /** @var \Drupal\wisski_pathbuilder\Entity\WisskiPathbuilderEntity[] */
    $pathbuilders = \Drupal::entityTypeManager()->getStorage("wisski_pathbuilder")->loadMultiple($machine_names);
    if (!$pathbuilders) {
      $this->output()->writeln("There is no pathbuilder with any of the following machine names:");
      $this->output()->writeln($machine_names);
      return;
    }
    foreach ($pathbuilders as $machine_name => $pathbuilder) {
      $filename = $machine_name . ".xml";
      file_put_contents($filename, $pathbuilder->toXML());
      // Get cwd so users know where to find the exports.
      $cwd = $this->getConfig()->cwd();
      $this->output()->writeln("Exported pathbuilder \"$machine_name\" to $cwd/$filename");
    }
  }

  /**
   * Import pathbuilder. This operates from the root web directory.
   *
   * @param string $filename
   *   The name of the pathbuilder file.
   * @param array $options
   *   The options.
   *
   * @option adapter The adapter that the pathbuilder belongs to (required).
   * @option import_mode Import mode. One of
   *   "keep": Keep the settings from the import file
   *   "no_fields": Do not create fields and bundles
   *   "new_fields": Create new fields and bundles,
   *   (optional, default: keep).
   * @option: name The name of the patbuilder, shown as Label in the interface.
   * @option: generate Generate bundles and fields. (optional, default=FALSE, value="required" format="boolean")
   *
   * @command wisski-pathbuilder:import
   *
   * @usage drush wisski-pathbuilder:import gemaeldesammlung.xml
   *   Import the pathbuilder from $WEB_ROOT/gemaeldesammlung.xml
   */
  public function importPathbuilder(
    string $filename,
    $options = ["adapter" => NULL, "import_mode" => "keep", "name" => NULL, 'generate' => FALSE],
  ) {
    $importModes = [
      "keep" => "keep",
      "no_fields" => WisskiPathbuilderEntity::CONNECT_NO_FIELD,
      "new_fields" => WisskiPathbuilderEntity::GENERATE_NEW_FIELD,
    ];

    // Sanity check import_mode.
    if (!in_array($options['import_mode'], $importModes)) {
      $this->output()->writeln("Only valid values for \"import_mode\" option are:");
      $this->output()->writeln(array_keys($importModes));
      return;
    }

    // Sanity check adapter.
    if (!$options['adapter']) {
      $this->output()->writeln("Please specify the adapter the pathbuilder should be attached to.");
      return;
    }

    $data = file_get_contents($filename);
    $id = str_replace(".xml", "", $filename);

    $adapter = $options["adapter"];

    $pathbuilder = WisskiPathbuilderEntity::fromXML($id, $data, $adapter, $options['import_mode'], $options["name"]);

    if ($options['generate']) {
      // Generate bundles and fields.
      $paths = $pathbuilder->getPbPaths();
      PathbuilderManager::saveAndGenerate($pathbuilder, $paths);
    }
  }

}
