<?php

namespace Drupal\wisski_pathbuilder;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\wisski_pathbuilder\Entity\WisskiPathbuilderEntity;
use Drupal\wisski_pathbuilder\Entity\WisskiPathEntity;
use Drupal\wisski_salz\Entity\Adapter;
use Drupal\wisski_salz\RdfSparqlUtil;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class PathbuilderManager.
 */
class PathbuilderManager {

  use StringTranslationTrait;

  /**
   * The Cache Backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The File System service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Pathbuilders that make use of a given adapter.
   *
   * @var array[]
   */
  private static $pbsForAdapter = NULL;

  /**
   * Pathbuilders that make use of a given bundle.
   *
   * @var array[]
   */
  private static $pbsUsingBundle = NULL;

  /**
   * Bundles with a given starting concept.
   *
   * @var array
   */
  private static $bundlesWithStartingConcept = NULL;

  /**
   * Image paths for a given pathbuilder.
   *
   * @var array[]
   */
  private static $imagePaths = NULL;

  /**
   * Field paths for a given pathbuilder.
   *
   * @var array[]
   */
  private static $fieldPaths = NULL;

  /**
   * Pathbuilders.
   *
   * $fieldPaths[$fieldId][$pbId] = array($paths...)
   *
   * @var array[]
   */
  private static $pbs = NULL;

  /**
   * Paths.
   *
   * @var array[]
   */
  private static $paths = NULL;


  /**
   * Request Stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The FileRepositoryInterface service instance.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected FileRepositoryInterface $file;

  /**
   * The Entity Type Manager service instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs form variables.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The translations service.
   * @param \Drupal\file\FileRepositoryInterface $file
   *   Performs file system operations and updates database records
   *   accordingly.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(
    TranslationInterface $stringTranslation,
    FileRepositoryInterface $file,
    EntityTypeManagerInterface $entityTypeManager,
    CacheBackendInterface $cache,
    MessengerInterface $messenger,
    FileSystemInterface $fileSystem,
    RequestStack $requestStack,
  ) {
    $this->stringTranslation = $stringTranslation;
    $this->file = $file;
    $this->entityTypeManager = $entityTypeManager;
    $this->cache = $cache;
    $this->messenger = $messenger;
    $this->fileSystem = $fileSystem;
    $this->requestStack = $requestStack;
  }

  /**
   * Reset the cached mappings.
   */
  public function reset() {
    self::$pbsForAdapter = NULL;
    self::$pbsUsingBundle = NULL;
    self::$imagePaths = NULL;
    self::$fieldPaths = NULL;
    self::$pbs = NULL;
    self::$paths = NULL;
    $this->cache->delete('wisski_pathbuilder_manager_pbs_for_adapter');
    $this->cache->delete('wisski_pathbuilder_manager_pbs_using_bundle');
    $this->cache->delete('wisski_pathbuilder_manager_image_paths');
    $this->cache->delete('wisski_pathbuilder_manager_field_paths');
  }

  /**
   * Get all pathbuilders.
   *
   * @return \Drupal\wisski_pathbuilder\Entity\WisskiPathbuilderEntity[]
   *   All pathbuilders.
   */
  private function getPbs(): array {
    // Not yet fetched from cache?
    if (self::$pbs === NULL) {
      if ($cache = $this->cache
        ->get('wisski_pathbuilder_manager_pbs')) {
        self::$pbs = $cache->data;
      }
    }
    // Was reset, recalculate.
    if (self::$pbs === NULL) {
      $pbs = WisskiPathbuilderEntity::loadMultiple();
      $cache = $this->cache->set('wisski_pathbuilder_manager_pbs', $pbs);
      self::$pbs = $pbs;
    }

    if (!empty(self::$pbs)) {
      return self::$pbs;
    }
    return NULL;

  }

  /**
   * Returns a pathbuilder for the given pathbuilder Id.
   *
   * Either gets it from the local cache or loads it
   * if it isn't cached.
   *
   * @return \Drupal\wisski_pathbuilder\Entity\WisskiPathbuilderEntity|null
   *   The pathbuilder or null if it does't exist.
   */
  public function getPathbuilder(string $id): WisskiPathbuilderEntity|NULL {
    $pbs = $this->getPbs();
    if (!isset($pbs[$id])) {
      return NULL;
    }
    return $pbs[$id];
  }

  /**
   * Returns all paths.
   *
   * @return \Drupal\wisski_pathbuilder\Entity\WisskiPathEntity[]
   *   All Path entities.
   */
  private function getPaths(): array {
    // Not yet fetched from cache?
    if (self::$paths === NULL) {
      $cache = $this->cache->get('wisski_pathbuilder_manager_paths');
      if ($cache) {
        self::$paths = $cache->data;
      }
    }
    // Was reset, recalculate.
    if (self::$paths === NULL) {
      $paths = WisskiPathEntity::loadMultiple();
      $cache = $this->cache->set('wisski_pathbuilder_manager_paths', $paths);
      self::$paths = $paths;
    }

    if (!empty(self::$paths)) {
      return self::$paths;
    }
    return NULL;
  }

  /**
   * Returns path for a given path id.
   *
   * @param string $id
   *   The path id.
   *
   * @return \Drupal\wisski_pathbuilder\Entity\WisskiPathEntity|null
   *   The wisski path entity or null if it doesn't exist.
   */
  public function getPath(string $id): WisskiPathEntity|NULL {
    $paths = $this->getPaths();
    if (!isset($paths[$id])) {
      return NULL;
    }
    return $paths[$id];
  }

  /**
   * Get the pathbuilders that make use of a given adapter.
   *
   * @param string $adapter_id
   *   The ID of the adapter.
   *
   * @return array
   *   If adapter_id is empty, returns an array where the keys are
   *   adapter IDs and the values are arrays of corresponding
   *   pathbuilders. If adapter_id is given returns an array of
   *   corresponding pathbuilders.
   */
  public function getPbsForAdapter($adapter_id = NULL) {
    // Not yet fetched from cache?
    if (self::$pbsForAdapter === NULL) {
      if ($cache = $this->cache
        ->get('wisski_pathbuilder_manager_pbs_for_adapter')) {
        self::$pbsForAdapter = $cache->data;
      }
    }
    // Was reset.
    if (self::$pbsForAdapter === NULL) {
      self::$pbsForAdapter = [];
      // $pbs = entity_load_multiple('wisski_pathbuilder');
      /** @var \Drupal\wisski_pathbuilder\Entity\WisskiPathbuilderEntity[] $pbs */
      $pbs = $this->entityTypeManager
        ->getStorage('wisski_pathbuilder')
        ->loadMultiple();

      foreach ($pbs as $pbid => $pb) {
        $adapter = $this->loadAdapterForPb($pb);
        if ($adapter) {
          $aid = $pb->getAdapterID();
          if (!isset(self::$pbsForAdapter[$aid])) {
            self::$pbsForAdapter[$aid] = [];
          }
          self::$pbsForAdapter[$aid][$pbid] = $pbid;
        }
        else {
          $this->messenger
            ->addError($this->t('Pathbuilder %pb refers to non-existing adapter with ID %aid.', [
              '%pb' => $pb->getName(),
              '%aid' => $pb->getAdapterId(),
            ]));
        }
      }
      $this->cache
        ->set('wisski_pathbuilder_manager_pbs_for_adapter', self::$pbsForAdapter);
    }
    return empty($adapter_id)
      ? self::$pbsForAdapter
      // If there is no pb for this adapter there is no array key.
      : (self::$pbsForAdapter[$adapter_id] ?? []);
  }

  /**
   * Get the pathbuilders that make use of a given bundle.
   *
   * @param string $bundle_id
   *   The ID of the bundle.
   */
  public function getPbsUsingBundle($bundle_id = NULL) {
    // Not yet fetched from cache?
    if (self::$pbsUsingBundle === NULL) {
      if ($cache = $this->cache
        ->get('wisski_pathbuilder_manager_pbs_using_bundle')) {
        self::$pbsUsingBundle = $cache->data;
      }
    }
    // Was reset, recalculate.
    if (self::$pbsUsingBundle === NULL) {
      $this->calculateBundlesAndStartingConcepts();
    }
    return empty($bundle_id)
      // If no bundle given, return all.
      ? self::$pbsUsingBundle
      : (self::$pbsUsingBundle[$bundle_id] ?? []);

  }

  /**
   * Get the preview image for a given entity.
   *
   * @param string $entity_id
   *   The ID of the entity.
   * @param string $bundle_id
   *   The ID of the bundle.
   * @param \Drupal\wisski_adapter\WisskiAdapterInterface $adapter
   *   The adapter.
   */
  public function getPreviewImage($entity_id, $bundle_id, $adapter) {
    $pbs_and_paths = $this->getImagePathsAndPbsForBundle($bundle_id);

    // dpm($pbs_and_paths, "yay!");.
    foreach ($pbs_and_paths as $pb_id => $paths) {

      $pbs = $this->getPbs();

      $pb = $pbs[$pb_id];

      /* Get the correct adapter so we dont do wrong queries... */
      if ($pb->getAdapterId() != $adapter->id()) {
        // dpm("wrong adapter");
        // dpm($adapter, "adap?");.
        continue;
      }

      $the_pathid = NULL;
      // Beat this ...
      $weight = 99999999999;

      // Go through all paths and look for the lowest weight.
      foreach ($paths as $key => $pathid) {
        $pbp = $pb->getPbPath($pathid);

        if (empty($pbp['enabled'])) {
          continue;
        }

        if (isset($pbp['weight'])) {
          if ($pbp['weight'] < $weight) {
            // Only take this if the weight is better or the same.
            $the_pathid = $pathid;
            $weight = $pbp['weight'];
            // $or_paths[$key] = $pathid;
          }
        }
        elseif (empty($the_pathid)) {
          // If there was nothing before, something is better at least.
          $the_pathid = $pathid;
        }
      }

      // dpm($pathid, "assa");.
      // Nothing found?
      if (empty($the_pathid)) {
        return [];
      }

      $paths = $this->getPaths();

      $path = $paths[$the_pathid];
      // dpm(microtime(), "ptr?");.
      $values = $adapter->getEngine()
        ->pathToReturnValue($path, $pb, $entity_id, 0, NULL, FALSE);
      // dpm(microtime(), "ptr!");.
      // Check for empty strings...
      if (!empty($values) && !empty(current($values))) {
        return $values;
      }
      else {
        // If we did not find anything in the "primary"
        // path we will have to look at others...
        /*
        foreach($paths as $key => $pathid) {
        if($pathid == $the_pathid)
        continue; // we already had that

        $path = $paths[$key];


        #          dpm($pathid, "looking at id ");
        #          dpm(serialize($path), "path is");
        if(!empty($path))
        $values = $adapter
        ->getEngine()
        ->pathToReturnValue($path, $pb, $entity_id, 0, NULL, FALSE);

        if(!empty($values) && !empty(current($values)))
        return $values;
        }
         */
      }

    }
    return [];
  }

  /**
   * Loads image path ids and corresponding pathbuilders.
   *
   * @param string $bundle_id
   *   The bundle id.
   *
   * @return array
   *   Image paths with corresponding pathbuilders.
   */
  public function getImagePathsAndPbsForBundle($bundle_id): array {

    // Not yet fetched from cache?
    if (self::$imagePaths === NULL) {
      if ($cache = $this->cache
        ->get('wisski_pathbuilder_manager_image_paths')) {
        self::$imagePaths = $cache->data;
      }
    }
    // Was reset, recalculate.
    if (self::$imagePaths === NULL) {
      $this->calculateImagePaths();
    }

    if (isset(self::$imagePaths[$bundle_id])) {
      return self::$imagePaths[$bundle_id];
    }

    return [];

  }

  /**
   * Loads image path ids and corresponding pathbuilders.
   */
  public function calculateImagePaths() {

    // $pbs = entity_load_multiple('wisski_pathbuilder');
    $pbs = $this->getPbs();

    foreach ($pbs as $pbid => $pb) {
      $groups = $pb->getMainGroups();

      foreach ($groups as $group) {
        $bundleid = $pb->getPbPath($group->id())['bundle'];
        $paths = $pb->getImagePathIDsForGroup($group->id());

        if (!empty($paths)) {
          self::$imagePaths[$bundleid][$pbid] = $paths;
        }

        // foreach($paths as $pathid) {
        // $path = WisskiPathEntity::load($pathid);
        // $info[$bundleid][$pbid][$pathid] = $pathid;
        // }.
      }
    }

    $this->cache
      ->set('wisski_pathbuilder_manager_image_paths', self::$imagePaths);
  }

  /**
   * Loads path ids and corresponding pathbuilders. for a given field type.
   *
   * @param string $field_id
   *   The field id.
   *
   * @return array
   *   Field paths as a mapping from pathbuilder ids to list of path objects
   */
  public function getPathIdsAndPbIdsForFieldId($field_id): array {

    // Not yet fetched from cache?
    if (self::$fieldPaths === NULL) {
      if ($cache = $this->cache
        ->get('wisski_pathbuilder_manager_field_paths')
      ) {
        self::$fieldPaths = $cache->data;
      }
    }
    // Was reset, recalculate.
    if (self::$fieldPaths === NULL) {
      $this->calculateFieldPaths();
    }

    if (isset(self::$fieldPaths[$field_id])) {
      return self::$fieldPaths[$field_id];
    }

    return [];
  }

  /**
   * Maps the paths to their corresponding bundle id and pathbuilder id.
   *
   * [<fieldId> => [pathbuilderId => pathId]]
   */
  public function calculateFieldPaths() {

    // $pbs = entity_load_multiple('wisski_pathbuilder');
    $pbs = $this->getPbs();

    $fieldPaths = [];
    foreach ($pbs as $pbid => $pb) {
      $pbpaths = $pb->getPbPaths();
      foreach ($pbpaths as $id => $potpath) {
        $fieldId = $potpath['field'];

        if (!isset($fieldPaths[$fieldId])) {
          $fieldPaths[$fieldId] = [];
        }
        if (!isset($fieldPaths[$fieldId][$pbid])) {
          $fieldPaths[$fieldId][$pbid] = [];
        }
        $fieldPaths[$fieldId][$pbid][] = $id;
      }
    }
    self::$fieldPaths = $fieldPaths;

    $this->cache
      ->set('wisski_pathbuilder_manager_field_paths', self::$fieldPaths);
  }

  /**
   * Get the bundles that have a starting concept.
   */
  public function getBundlesWithStartingConcept($concept_uri = NULL) {
    // Not yet fetched from cache?
    if (self::$bundlesWithStartingConcept === NULL) {
      if ($cache = $this->cache
        ->get('wisski_pathbuilder_manager_bundles_with_starting_concept')) {
        self::$bundlesWithStartingConcept = $cache->data;
      }
    }
    // Was reset, recalculate.
    if (self::$bundlesWithStartingConcept === NULL) {
      $this->calculateBundlesAndStartingConcepts();
    }
    return empty($concept_uri)
      // If no concept given, return all.
      ? self::$bundlesWithStartingConcept
      : (self::$bundlesWithStartingConcept[$concept_uri] ?? []);

  }

  /**
   * Loads the adapter for the given pathbuilder, or NULL if it does not exist .*/
  private function loadAdapterForPb($pb) {
    if (is_null($pb)) {
      return NULL;
    }

    $id = $pb->getAdapterId();
    // Some linkblocks are horribly broken, and have a NULL here.
    if (!is_string($id)) {
      return NULL;
    }

    $storage = $this->entityTypeManager->getStorage('wisski_salz_adapter');
    return $storage->load($id);
  }

  /**
   * Calculates the bundles that have a starting concept.
   */
  private function calculateBundlesAndStartingConcepts() {
    self::$pbsUsingBundle = [];
    self::$bundlesWithStartingConcept = [];

    $pbs = $this->getPbs();

    foreach ($pbs as $pbid => $pb) {
      foreach ($pb->getAllGroups() as $group) {
        $pbpath = $pb->getPbPath($group->getID());
        $bid = $pbpath['bundle'];
        if (!empty($bid)) {
          if (!isset(self::$pbsUsingBundle[$bid])) {
            self::$pbsUsingBundle[$bid] = [];
          }
          /** @var \Drupal\wisski_salz\Entity\WisskiSalzAdapterEntity $adapter */
          $adapter = $this->loadAdapterForPb($pb);
          if ($adapter) {
            // Struct for pbsUsingBundle.
            if (!isset(self::$pbsUsingBundle[$bid][$pbid])) {
              $engine = $adapter->getEngine();
              $info = [
                'pb_id' => $pbid,
                'adapter_id' => $adapter->id(),
                'writable' => $engine->isWritable(),
                'preferred_local' => $engine->isPreferredLocalStore(),
                'engine_plugin_id' => $engine->getPluginId(),
                // Filled below.
                'main_concept' => [],
                // Filled below.
                'is_top_concept' => [],
                // Filled below.
                'groups' => [],
              ];
              self::$pbsUsingBundle[$bid][$pbid] = $info;
            }
            $path_array = $group->getPathArray();
            // The last concept is the main concept.
            $main_concept = end($path_array);
            self::$pbsUsingBundle[$bid][$pbid]['main_concept'][$main_concept] = $main_concept;
            if (empty($pbpath['parent'])) {
              self::$pbsUsingBundle[$bid][$pbid]['is_top_concept'][$main_concept] = $main_concept;
            }
            self::$pbsUsingBundle[$bid][$pbid]['groups'][$group->getID()] = $main_concept;

            // Struct for bundlesWithStartingConcept.
            if (!isset(self::$bundlesWithStartingConcept[$main_concept])) {
              self::$bundlesWithStartingConcept[$main_concept] = [];
            }
            if (!isset(self::$bundlesWithStartingConcept[$main_concept][$bid])) {
              self::$bundlesWithStartingConcept[$main_concept][$bid] = [
                'bundle_id' => $bid,
                'is_top_bundle' => FALSE,
                'pb_ids' => [],
                'adapter_ids' => [],
              ];
            }
            self::$bundlesWithStartingConcept[$main_concept][$bid]['pb_ids'][$pbid] = $pbid;
            self::$bundlesWithStartingConcept[$main_concept][$bid]['adapter_ids'][$adapter->id()] = $adapter->id();
            if (empty($pbpath['parent'])) {
              self::$bundlesWithStartingConcept[$main_concept][$bid]['is_top_bundle'] = TRUE;
            }

          }
          else {
            $this->messenger
              ->addError($this->t('Pathbuilder %pb refers to non-existing adapter with ID %aid.', [
                '%pb' => $pb->getName(),
                '%aid' => $pb->getAdapterId(),
              ]));
          }
        }
      }
    }
    $this->cache
      ->set('wisski_pathbuilder_manager_pbs_using_bundle', self::$pbsUsingBundle);
    $this->cache
      ->set('wisski_pathbuilder_manager_bundles_with_starting_concept', self::$bundlesWithStartingConcept);
  }

  /**
   * Get the orphaned paths.
   */
  public function getOrphanedPaths() {

    // $pba = entity_load_multiple('wisski_pathbuilder');
    // $pa = entity_load_multiple('wisski_path');
    $pba = $this->entityTypeManager
      ->getStorage('wisski_pathbuilder')
      ->loadMultiple();
    $pa = $this->entityTypeManager
      ->getStorage('wisski_path')
      ->loadMultiple();

    // Filled in big loop.
    $tree_path_ids = [];

    // Here go regular paths, ie. that are in a pb's path tree.
    $home = [];
    // Here go paths that are listed in a pb but not in its
    // path tree (are "hidden").
    $semiorphaned = [];
    // Here go paths that aren't mentioned in any pb.
    $orphaned = [];

    foreach ($pa as $pid => $p) {
      $is_orphaned = TRUE;
      /** @var \Drupal\wisski_adapter\WisskiPathbuilderEntity $pb */
      foreach ($pba as $pbid => $pb) {
        if (!isset($tree_path_ids[$pbid])) {
          $tree_path_ids[$pbid] = $this->getPathIdsInPathTree($pb);
        }
        $pbpath = $pb->getPbPath($pid);
        if (isset($tree_path_ids[$pbid][$pid])) {
          $home[$pid][$pbid] = $pbid;
          $is_orphaned = FALSE;
        }
        elseif (!empty($pbpath)) {
          $semiorphaned[$pid][$pbid] = $pbid;
          $is_orphaned = FALSE;
        }
      }
      if ($is_orphaned) {
        $orphaned[$pid] = $pid;
      }
    }
    return [
      'home' => $home,
      'semiorphaned' => $semiorphaned,
      'orphaned' => $orphaned,
    ];

  }

  /**
   * Get all path ids in a path tree.
   */
  public function getPathIdsInPathTree($pb) {
    $ids = [];
    $agenda = $pb->getPathTree();
    while ($node = array_shift($agenda)) {
      $ids[$node['id']] = $node['id'];
      $agenda = array_merge($agenda, $node['children']);
    }
    return $ids;
  }

  /**
   * Prepare export directory structure.
   *
   * @param string $exportRootDir
   *   The directory path, where to store the export files,
   *   i.e. defined as a const in ExportAllConfirmForm.
   *
   * @return string
   *   The relative path to the current export directory.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function prepareExportDirectories(string $exportRootDir) {
    // Prepare file structure.
    $relativeExportDir = $exportRootDir . date('Ymd') . '/';
    $instanceDir = $relativeExportDir . $this->requestStack->getCurrentRequest()->getHttpHost();
    $ontologiesDir = $instanceDir . '/ontologies/';
    $pathbuildersDir = $instanceDir . '/pathbuilders/';
    $directoryTree = [
      'relativeExportDir' => $relativeExportDir,
      'instanceDir' => $instanceDir,
      'ontologiesDir' => $ontologiesDir,
      'pathbuilderDir' => $pathbuildersDir,
    ];
    foreach ($directoryTree as $key => $value) {
      $preparedExportDirectory = $this->fileSystem
        ->prepareDirectory($value, FileSystemInterface::CREATE_DIRECTORY);
      // If folder is not writable, escape.
      if (!$preparedExportDirectory) {
        $this->messenger
          ->addError($this->t('Could not create archive at %relativeExportDirectory. Do you have the right permissions?', ['%relativeExportDirectory' => $relativeExportDir]));
        return FALSE;
      }
    }

    return $directoryTree;
  }

  /**
   * Saves all ontologies.
   *
   * @param string $ontologiesDir
   *   The directory path, where to store the export files,
   *   i.e. defined as a const in ExportAllConfirmForm.
   *
   * @return bool
   *   Sucess of the export.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function exportAllOntologies(string $ontologiesDir) {

    // Counter for existing ontologies.
    $count = 0;

    // Load adapters.
    $adapters = Adapter::loadMultiple();

    // Iterate over adapters and find the ontologies.
    foreach ($adapters as $adapter) {

      // Load the engine of the adapter.
      /** @var \Drupal\wisski_adapter\WisskiSalzEngineInterface $engine */
      $engine = $adapter->getEngine();

      if (!method_exists($engine, 'directQuery')) {
        $this->messenger->addWarning($this->t('Engine @engine (of adapter @adapter) does not support ontology export, skipping.', [
          '@engine' => get_class($engine),
          '@adapter' => $adapter->id(),
        ]));
        continue;
      }

      $query = '
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX owl: <http://www.w3.org/2002/07/owl#>
      SELECT ?s1 ?p1 ?o1 ?g
        WHERE {
          GRAPH ?g { ?s rdf:type owl:Ontology .
          } .
          GRAPH ?g { ?s1 ?p1 ?o1 .
          }
        }';
      $results = $engine->directQuery($query);
      $count += 1;

      // Create a nq-file for every Ontology.
      foreach ($results as $nquad) {
        // Parse stdClass to php array.
        $sparqlEntityArray = [];
        foreach ($nquad as $sparqlEntity) {
          $sparqlEntityArray[] = $sparqlEntity;
        }
        // Convert SPARQL entities to statement string and add to array.
        $quadStatement[] = implode(" ", array_map([$this, 'sparqlEntityStringlifier'], $sparqlEntityArray));
      }
      if (empty($quadStatement)) {
        $this->messenger
          ->addWarning($this->t('Found no statements, do you have an ontology?'));
        return FALSE;
      }
      // Parse statement array to string.
      $nQuads = implode(" . \n", $quadStatement) . ' .';

      // Write file to disk.
      $fileName = 'ontology_' . $count . '.nq';
      $export_path = $ontologiesDir . $fileName;
      $this->file->writeData($nQuads, $export_path, FileSystemInterface::EXISTS_REPLACE);
    }
    $this->messenger
      ->addMessage($this->t('Exported all ontologies.'));
    return TRUE;
  }

  /**
   * Zips all ontologies and pathbuilders.
   *
   * Takes a list of files and add them to a zip archive.
   *
   * @param string $relativeExportDir
   *   The relative export Dir.
   * @param array $zipFiles
   *   List of files to zip.
   */
  public function zipPathbuildersAndOntologies(string $relativeExportDir, array $zipFiles) {
    // If there is no export dir or zip files escape.
    if (!$relativeExportDir || !$zipFiles) {
      return FALSE;
    }
    $absoluteZipDirPath = $this->fileSystem
      ->realpath($relativeExportDir);

    // Open zip file.
    $zipPath = $absoluteZipDirPath . '.zip';
    $zip = new \ZipArchive();
    $zip->open($zipPath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

    foreach ($zipFiles as $zipFile) {
      $internalZipPath = explode('/', $zipFile, 5)[4];
      $absoluteZipFilePath = $this->fileSystem
        ->realpath($zipFile);
      $zip->addFile($this->fileSystem
        ->realpath($absoluteZipFilePath), $internalZipPath);
      $this->messenger
        ->addMessage($this->t('Zipped files in archive %zipPath.', ['%zipPath' => $zipPath]));

    }
    $zip->close();
    return TRUE;
  }

  /**
   * Convert EasyRdf objects to strings.
   *
   * @param object $sparqlEntity
   *   Ether EasyRdf\Resource or EasyRdf\Literal.
   */
  public function sparqlEntityStringlifier(object $sparqlEntity) {
    // If it is a ressource bracket it in angle brackets.
    if (get_class($sparqlEntity) == "EasyRdf\Resource") {
      return "<" . $sparqlEntity->getUri() . ">";
    }
    else {
      // Quote it and add language flag.
      $literal = $sparqlEntity->getValue();
      $escapedLiteral = (new RdfSparqlUtil)->escapeSparqlLiteral($literal);
      return '"' . $escapedLiteral . '"' . (empty($sparqlEntity->getLang()) ? '' : "@" . $sparqlEntity->getLang());
    }
  }

  /**
   * Exports pathbuilder structure.
   *
   * @param \Drupal\wisski_pathbuilder\Entity\WisskiPathbuilderEntity $pathbuilderEntity
   *   The pathbuilder entity.
   * @param string $pathbuildersDir
   *   A folder with the current date inside the EXPORT_ROOT_DIR
   *   containing ontologies and pathbuilders.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function exportPathbuilder(WisskiPathbuilderEntity $pathbuilderEntity, string $pathbuildersDir) {
    $xml = $pathbuilderEntity->toXML();

    // Save the files.
    $export_path = $pathbuildersDir . 'pathbuilder_' . $pathbuilderEntity->id();
    $this->file->writeData($xml, $export_path, FileExists::Replace);
  }

  /**
   * Exports all pathbuilders.
   *
   * @param string $relativeExportDirectory
   *   The directory path, where to store the export files,
   *   i.e. defined as a const in ExportAllConfirmForm.
   *
   * @return bool
   *   Sucess of the export.
   *
   * @throws \Exception
   */
  public function exportAllPathbuilders(string $relativeExportDirectory) {
    $wisskiPathbuilderIds = $this->entityTypeManager->getStorage('wisski_pathbuilder')->getQuery()
      ->accessCheck(TRUE)
      ->execute();
    foreach ($wisskiPathbuilderIds as $pathbuilderId) {
      $pathbuilderEntity = ($this->entityTypeManager->getStorage('wisski_pathbuilder')->load($pathbuilderId));
      $this->exportPathbuilder($pathbuilderEntity, $relativeExportDirectory);
    }
    $this->messenger
      ->addMessage($this->t('Exported all pathbuilders.'));
    return TRUE;
  }

  /**
   * Removes Directories recursively.
   *
   * @param string $dir
   *   The directory to remove.
   */
  public function rRmDir(string $dir) {
    if (is_dir($dir)) {
      $objects = scandir($dir);
      foreach ($objects as $object) {
        if ($object != "." && $object != "..") {
          if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object)) {
            $this->rRmDir($dir . DIRECTORY_SEPARATOR . $object);
          }
          else {
            unlink($dir . DIRECTORY_SEPARATOR . $object);
          }
        }
      }
      rmdir($dir);
      $this->messenger
        ->addMessage($this->t('Removed temporary files and folders.'));
    }
  }

  /**
   * Collect files for Zip archive.
   *
   * @param string $dir
   *   The directory to search for files to zip.
   * @param array $zipFiles
   *   The files to add to the zip archive.
   */
  public function collectZipDirs(string $dir, array &$zipFiles) {
    if (is_dir($dir)) {
      $objects = scandir($dir);
      foreach ($objects as $object) {
        if ($object != "." && $object != "..") {
          $this->collectZipDirs($dir . DIRECTORY_SEPARATOR . $object, $zipFiles);
          if (is_file($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object)) {
            $zipFiles[] = $dir . DIRECTORY_SEPARATOR . $object;
          }
        }
      }
    }
  }

  /**
   * Save Pathbuilder and generate fields and bundles.
   *
   * @param \Drupal\wisski_pathbuilder\Entity\WisskiPathbuilderEntity $pathbuilder
   *   The pathbuilder entity.
   * @param array $paths
   *   The paths to save.
   */
  public static function saveAndGenerate(WisskiPathbuilderEntity $pathbuilder, array $paths = []) {
    // Iterate all pbpaths and look for damaged ones
    // delete these before save.
    $pbpaths = $pathbuilder->getPbPaths();
    foreach ($pbpaths as $key => $pbpath) {
      if (empty($pbpath)) {
        unset($pbpaths[$key]);
      }
    }
    $pathbuilder->setPbPaths($pbpaths);

    // dpm($paths, "paths");.
    if (!empty($paths)) {
      foreach ($paths as $key => $path) {

        if ($path['enabled'] == 0) {

          $pathob = WisskiPathEntity::load($path['id']);

          $pbpath = $pathbuilder->getPbPath($pathob->id());

          $field = NULL;

          if ($pathob->isGroup()) {
            $field = $pbpath['bundle'];
          }
          else {
            $field = $pbpath['field'];
          }

          // Delete old fields.
          $field_storages = \Drupal::entityTypeManager()->getStorage('field_storage_config')->loadByProperties(['field_name' => $field]);
          if (!empty($field_storages)) {
            foreach ($field_storages as $field_storage) {
              $field_storage->delete();
            }
          }

          $field_objects = \Drupal::entityTypeManager()->getStorage('field_config')->loadByProperties(['field_name' => $field]);
          if (!empty($field_objects)) {
            foreach ($field_objects as $field_object) {
              $field_object->delete();
            }
          }

          continue;

        }

        if (!empty($path['parent']) && $paths[$path['parent']]['enabled'] == 0) {
          // Take it with us if the parent is disabled down the tree.
          $paths[$key]['enabled'] = 0;

          continue;
        }

        // drupal_set_message($path['id']);.
        // Generate fields!
        $pathob = WisskiPathEntity::load($path['id']);

        if ($pathob->isGroup()) {
          // Save the original bundle id because
          // if it is overwritten in create process
          // we won't have it anymore.
          $pbpaths = $pathbuilder->getPbPaths();

          // Which group should I handle?
          $my_group = $pbpaths[$pathob->id()];

          // Original bundle.
          $ori_bundle = $my_group['bundle'];

          $pathbuilder->generateBundleForGroup($pathob->id());
          if (!in_array($pathob->id(), array_keys($pathbuilder->getMainGroups()))) {
            $pathbuilder->generateFieldForSubGroup($pathob->id(), $pathob->getName(), $ori_bundle);
          }
        }
        else {
          // dpm($pathob,'$pathob');.
          $pathbuilder->generateFieldForPath($pathob->id(), $pathob->getName());
        }

      }
      $pathbuilder->save();
    }
  }

}
