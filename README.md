# WissKI

WissKI is a German acronym for "Wissenschaftliche
KommunikationsInfrastruktur", which can be translated as "Scientific
Communication Infrastructure". It is a Virtual Research Environment (VRE)
for managing scholarly data that is completely open source and free to use.
Based on Drupal WissKI is a flexible tool for data collections that enables
researchers to work simultaneously from different places. WissKI provides
benefits of the semantic web technology while supporting simple and common
input interfaces. The semi-automatic text annotation is one of its core
features. We developed a software system that enables scientific projects
especially in memory institutions (museums, archives, libraries) to collect,
store, manage and communicate knowledge. Therefore it addresses many facets
of research in a network environment like persistence of information,
long-time preservation and accessibility, digital documentation standards
and e-publishing.

WissKI was initially a joint venture featuring three partners from different
institutions and scientific domains: The Digital Humanities Research Group
of the Department of Computer Science at the Friedrich-Alexander-University
of Erlangen-Nuremberg (FAU), the Department of Museum Informatics at the
Germanisches Nationalmuseum (GNM) in Nuremberg and the Biodiversity
Informatics Group at the Zoologisches Forschungsmuseum Alexander Koenig
(ZFMK) in Bonn. Software development was primarily funded by the German
Research Foundation (Deutsche Forschungsgemeinschaft, DFG) from 2009 to 2012
and from 2014 to 2017.

- For a full description of the module, visit the
  [project page](https://www.drupal.org/project/wisski).

- To submit bug reports and feature suggestions, or track changes in the
  [issue queue](https://www.drupal.org/project/issues/wisski).


## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Troubleshooting
- Maintainers


## Requirements

This module requires the following modules:

- [Inline Entity Form](https://www.drupal.org/project/inline_entity_form).


## Recommended modules

- [Actions](https://www.drupal.org/project/actions).
- [File Metadata Manager](https://www.drupal.org/project/file_mdm).
- [Imagemagick](https://www.drupal.org/project/imagemagick).
- [Geofield](https://www.drupal.org/project/geofield).
- [Geofield Google Map](https://www.drupal.org/project/geofield_map).
- [Search API](https://www.drupal.org/project/search_api).
- [Colorbox](https://www.drupal.org/project/colorbox).
- [Image Effects](https://www.drupal.org/project/image_effects).
- [Devel](https://www.drupal.org/project/devel).


## Installation

- Install as you would normally install a contributed Drupal module. for further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

- A complete guide can be found here: http://wiss-ki.eu/installation_guide


## Configuration

Configuration depends on your use case. We provide a full guide on our
website https://wiss-ki.eu and in the projects documentation.

- Please [visit our complete guide](https://wiss-ki.eu/documentation) for more information.


## Troubleshooting

- If nothing happens, the typical strategy is: clear cache
- For further help you can join the
[slack channel](https://join.slack.com/t/wisski/shared_invite/zt-pvn8kumb-LQtezANcVmn47r_bY4FH9w).


## Maintainers

- [Mark Fichtner (Knurg)](https://www.drupal.org/u/knurg)
- [Robert Nasarek (rnsrk)](https://www.drupal.org/u/rnsrk)

The Project has been sponsored by the german research council (DFG,
http://www.dfg.de) and is supported by the the FAU, GNM and ZFMK.

Former contributers (At least 10 valid commits):
- Martin Scholz
- Georg Hohmann
- Kerstin Reinfandt
- Dorian Merz

## License

WissKI and related modules are, unless otherwise noted, available under the terms of GPL2.0 or later as below.
You can find the full license text in [gpl-2.0.txt](gpl-2.0.txt).

```
Copyright (C) 2008-2025 WissKI Contributors

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <https://www.gnu.org/licenses>.
```
