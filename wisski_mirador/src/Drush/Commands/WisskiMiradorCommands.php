<?php

namespace Drupal\wisski_mirador\Drush\Commands;

use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Filesystem\Filesystem;


/**
 * A Drush commandfile.
 */
final class WisskiMiradorCommands extends DrushCommands {

  use AutowireTrait;

  /**
   * Library discovery service.
   *
   * @var Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * Constructs a WisskiMiradorCommands object.
   */
  public function __construct(LibraryDiscoveryInterface $library_discovery) {
    $this->libraryDiscovery = $library_discovery;
    parent::__construct();
  }

  /**
   * Download and install the WissKI Mirador Integration plugin.
   *
   * @command wisski-mirador:wisski-mirador-integration
   *
   * @options release
   *  The release number of the WissKI Mirador Integration plugin to download.
   * @usage drush wisski-mirador:wisski-mirador-integration --release=1.1
   *  Download and install the WissKI Mirador Integration plugin (with release 1.1)
   * @aliases w-imv
   */
  public function download(
    $options = [
      'release' => 'main',
    ]) {
    $fs = new Filesystem();
    $path = DRUPAL_ROOT . '/libraries/wisski-mirador-integration';

    if ($options['release'] === 'main') {
      $url = 'https://github.com/rnsrk/wisski-mirador-integration/archive/main.zip';
    }
    else {
      $url = 'https://github.com/rnsrk/wisski-mirador-integration/archive/refs/tags/' . $options['release'] . '.zip';
    }

    // Create path if it doesn't exist
    // Exit with a message otherwise.
    if (!$fs->exists($path)) {
      $fs->mkdir($path);
    }
    else {
      $this->logger()->notice(dt('WissKI Mirador Integration is already present at @path. No download required.', ['@path' => $path]));
      return;
    }

    // Download the file.
    $client = new Client();
    $destination = tempnam(sys_get_temp_dir(), 'wisski-mirador-integration-tmp');
    try {
      $client->get($url, ['sink' => $destination]);
    }
    catch (RequestException $e) {
      // Remove the directory.
      $fs->remove($path);
      $this->logger()->error(dt('Drush was unable to download the WissKI Mirador Integration library from @remote. @exception', [
        '@remote' => $url,
        '@exception' => $e->getMessage(),
      ]));
      return;
    }

    // Move downloaded file.
    $fs->rename($destination, $path . '/wisski-mirador-integration.zip');

    // Unzip the file.
    $zip = new \ZipArchive();
    $res = $zip->open($path . '/wisski-mirador-integration.zip');
    if ($res === TRUE) {
      $zip->extractTo($path);
      $zip->close();
    }
    else {
      // Remove the directory if unzip fails and exit.
      $fs->remove($path);
      $this->logger()->error(dt('Error: unable to unzip WissKI Mirador Integration file.', []));
      return;
    }

    // Remove the downloaded zip file.
    $fs->remove($path . '/wisski-mirador-integration.zip');

    // Move the file.
    $fs->mirror($path . '/wisski-mirador-integration-' . $options['release'], $path, NULL, ['override' => TRUE]);
    $fs->remove($path . '/wisski-mirador-integration-' . $options['release']);

    // Success.
    $this->logger()->success(dt('The WissKI Mirador Integration library has been successfully downloaded to @path.', [
      '@path' => $path,
    ]));
  }
}
