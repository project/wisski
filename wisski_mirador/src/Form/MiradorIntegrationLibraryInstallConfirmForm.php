<?php

namespace Drupal\wisski_mirador\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Provides a confirm form for installing the library.
 */
class MiradorIntegrationLibraryInstallConfirmForm extends ConfirmFormBase {

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mirador_integration_library_install_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to install the Mirador Integration library?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Install');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return \Drupal\Core\Url::fromRoute('system.admin');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fs = new Filesystem();
    $path = DRUPAL_ROOT . '/libraries/wisski-mirador-integration';
    $url = 'https://github.com/rnsrk/wisski-mirador-integration/archive/main.zip';


    // Create path if it doesn't exist
    // Exit with a message otherwise.
    if (!$fs->exists($path)) {
      $fs->mkdir($path);
    }
    else {
      $this->messenger(t('WissKI Mirador Integration is already present at @path. No download required.', ['@path' => $path]));
      return;
    }

    // Download the file.
    $client = new Client();
    $destination = tempnam(sys_get_temp_dir(), 'wisski-mirador-integration-tmp');
    try {
      $client->get($url, ['sink' => $destination]);
    }
    catch (RequestException $e) {
      // Remove the directory.
      $fs->remove($path);
      $this->messenger(t('Was unable to download the WissKI Mirador Integration library from @remote. See logs for more.', [
        '@remote' => $url,
      ]));
      $this->logger('wisski-mirador')->error(dt('Was unable to download the WissKI Mirador Integration library from @remote. @exception', [
        '@remote' => $url,
        '@exception' => $e->getMessage(),
      ]));
      return;
    }

    // Move downloaded file.
    $fs->rename($destination, $path . '/wisski-mirador-integration.zip');

    // Unzip the file.
    $zip = new \ZipArchive();
    $res = $zip->open($path . '/wisski-mirador-integration.zip');
    if ($res === TRUE) {
      $zip->extractTo($path);
      $zip->close();
    }
    else {
      // Remove the directory if unzip fails and exit.
      $fs->remove($path);
      $this->messenger(t('Error: unable to unzip WissKI Mirador Integration file.', []));
      $this->logger('wisski-mirador')->error(t('Error: unable to unzip WissKI Mirador Integration file.'));
      return;
    }

    // Remove the downloaded zip file.
    $fs->remove($path . '/wisski-mirador-integration.zip');

    // Move the file.
    $fs->mirror($path . '/wisski-mirador-integration-main', $path, NULL, ['override' => TRUE]);
    $fs->remove($path . '/wisski-mirador-integration-main');

    // Success.
    $this->messenger(t('The WissKI Mirador Integration library has been successfully downloaded to @path.', [
      '@path' => $path,
    ]));
    $form_state->setRedirectUrl(\Drupal\Core\Url::fromUri('base:/admin/help/wisski_mirador', ['absolute' => TRUE]));
  }

}
