<?php

namespace Drupal\wisski_mirador\Plugin\views\style;

use Drupal\core\form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;


/**
* Style plugin to render a mirador viewer as a
* views display style.
*
* @ingroup views_style_plugins
*
* @ViewsStyle(
*   id = "wisskimirador",
*   title = @Translation("WissKI Mirador"),
*   help = @Translation("The WissKI Mirador views Plugin"),
*   theme = "views_view_wisskimirador",
*   display_types = { "normal" }
* )
*/
class WisskiMirador extends StylePluginBase {
  
  /**
  * {@inheritdoc}
  */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['path'] = array('default' => 'wisski_mirador');
    $options['enable_annotations'] = array('default' => "");
    $options['entity_type_for_annotation'] = array('default' => "");
    $options['bundle_for_annotation'] = array('default' => "");
    $options['field_for_annotation_uuid'] = array('default' => "");
    $options['field_for_annotation_json'] = array('default' => "");
    $options['field_for_annotation_entity'] = array('default' => "");
    $options['field_for_annotation_text'] = array('default' => "");
    $options['field_for_annotation_fragment_selector'] = array('default' => "");
    $options['field_for_annotation_svg'] = array('default' => "");
    $options['field_for_annotation_source_reference'] = array('default' => "");
    $options['field_for_image_ids'] = array('default' => "");
    $options['field_for_label'] = array('default' => "");
    $options['window_settings'] = array('default' => '{
            "allowClose": false,
            "allowFullscreen": true,
            "allowMaximize": false,
            "allowTopMenuButton": true,
            "allowWindowSideBar": true,
            "sideBarPanel": "info",
            "defaultSideBarPanel": "attribution",
            "sideBarOpenByDefault": false,
            "defaultView": "single",
            "forceDrawAnnotations": true,
            "hideWindowTitle": true,
            "highlightAllAnnotations": false,
            "imageToolsEnabled": true,
            "showLocalePicker": true,
            "sideBarOpen": true,
            "switchCanvasOnSearch": true,
            "panels": {
              "info": true,
              "attribution": true,
              "canvas": true,
              "annotations": true,
              "search": true,
              "layers": true
            }
      }');
    
    return $options;
  }
  
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    
    $form['enable_annotations'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Annotations'),
      '#description' => $this->t('This enables annotation storage with Mirador. Annotations are stored locally and you have to define a mapping in Drupal to an associated entity here.'),
      '#default_value' => $this->options['enable_annotations'],
    ];
    
    $form['entity_type_for_annotation'] = [
      '#title' => $this->t('Entity type for annotations'),
      '#description' => $this->t('The machine name of the entity type the annotations are stored to.'),
      '#type' => 'textfield',
      '#size' => '50',
      '#default_value' => $this->options['entity_type_for_annotation'],
    ];
    
    $form['bundle_for_annotation'] = [
      '#title' => $this->t('Bundle for annotations'),
      '#description' => $this->t('The machine id of the bundle the annotations are stored to. The bundle needs fields to store the annotations which are defined below.'),
      '#type' => 'textfield',
      '#size' => '50',
      '#default_value' => $this->options['bundle_for_annotation'],
    ];
    
    $form['field_for_annotation_uuid'] = [
      '#title' => $this->t('Field for annotation uuid'),
      '#description' => $this->t('The machine id of the field for the uuid of the annotation. This is needed for searching etc.'),
      '#type' => 'textfield',
      '#size' => '50',
      '#default_value' => $this->options['field_for_annotation_uuid'],
    ];

    $form['field_for_annotation_entity'] = [
      '#title' => $this->t('Field for annotation entity'),
      '#description' => $this->t('This field (machine name) stores the entity that is annotated (usually the filename of the image file).'),
      '#type' => 'textfield',
      '#size' => '50',
      '#default_value' => $this->options['field_for_annotation_entity'],
    ];

    $form['field_for_annotation_source_reference'] = [
      '#title' => $this->t('Field for annotation reference'),
      '#description' => $this->t('This field (machine name) stores the source (canvas) manifest the annotation refers to. Usualy a json file.'),
      '#type' => 'textfield',
      '#size' => '50',
      '#default_value' => $this->options['field_for_annotation_source_reference'],
    ];
    
    $form['field_for_annotation_text'] = [
      '#title' => $this->t('Field for annotation text'),
      '#description' => $this->t('The machine id of the field for the text of the annotation.'),
      '#type' => 'textfield',
      '#size' => '50',
      '#default_value' => $this->options['field_for_annotation_text'],
    ];
    
    $form['field_for_annotation_svg'] = [
      '#title' => $this->t('Field for annotation svg'),
      '#description' => $this->t('The machine id of the field for the svg of the annotation.'),
      '#type' => 'textfield',
      '#size' => '50',
      '#default_value' => $this->options['field_for_annotation_svg'],
    ];
    

    $form['field_for_annotation_fragment_selector'] = [
      '#title' => $this->t('Field for fragment box of the annotation '),
      '#description' => $this->t('This field (machine name) stores coordinates of the fragment box where the annotation svg is drawed (x,y,width,height from the left upper corner).'),
      '#type' => 'textfield',
      '#size' => '50',
      '#default_value' => $this->options['field_for_annotation_fragment_selector'],
    ];

    $form['field_for_annotation_json'] = [
      '#title' => $this->t('Field for annotation json dump'),
      '#description' => $this->t('This field (machine id) serves as a data dump for the whole annotation json.'),
      '#type' => 'textfield',
      '#size' => '50',
      '#default_value' => $this->options['field_for_annotation_json'],
    ];

    $form['field_for_image_ids'] = [
      '#title' => $this->t('Field for image ids'),
      '#description' => $this->t('This field (machine name) stores the referred image ids. Only fill this if you know what you\'re doing. WissKI will not load the full entity if you use this.'),
      '#type' => 'textfield',
      '#size' => '50',
      '#default_value' => $this->options['field_for_image_ids'],
    ];
    
    $form['field_for_label'] = [
      '#title' => $this->t('Field for label'),
      '#description' => $this->t('This field (machine name) stores the label. Only fill this if you know what you\'re doing. WissKI will not load the full entity if you use this.'),
      '#type' => 'textfield',
      '#size' => '50',
      '#default_value' => $this->options['field_for_label'],
    ];
    
    $form['field_for_uri'] = [
      '#title' => $this->t('Field for uri'),
      '#description' => $this->t('This field (machine name) stores the uri. Only fill this if you know what you\'re doing. WissKI will not load the full entity if you use this.'),
      '#type' => 'textfield',
      '#size' => '50',
      '#default_value' => $this->options['field_for_uri'],
    ];
    
    $form['window_settings'] = [
      '#title' => $this->t('window'),
      '#description' => $this->t('Settings for Mirador "window" parameter'),
      '#type' => 'textarea',
      '#default_value' => $this->options['window_settings'],
    ];
    
    return $form;
  }
  
  public function usesFields() {
    return TRUE;
  }
  
  public function validate() {
    $fields = FALSE;
    
    $errors = array();
    
    foreach ($this->displayHandler->getHandlers('field') as $field) {
      if (empty($field->options['exclude'])) {
        $fields = TRUE;
      }
    }
    
    if (!$fields) {
      $errors[] = $this->t('No fields are selected for display purpose. For Mirador you either need to display entity id (then the manifests get loaded automatically) or you need to display a field with a reference (http...) to an IIIF manifest.');
    }
    
    if(empty($errors))
    parent::validate();
    
    return $errors;
  }
  
  /**
  * Renders the View.
  */
  public function render() {
    
    $cache = \Drupal::cache('wisski_iiif_params');
    
    $view = $this->view;
    
    $results = $view->result;
    
    $ent_list = array();
    $direct_load_list = array();
    
    $site_config = \Drupal::config('system.site');
    
    $to_print = "";
    
    if(!empty($site_config->get('name')))
    $to_print .= $site_config->get('name');
    if(!$site_config->get('slogan')) {
      if(!empty($to_print) && !empty($site_config->get('slogan'))) {
        $to_print .= " (" . $site_config->get('slogan') . ") ";
      } else {
        $to_print .= $site_config->get('slogan');
      }
    }
    
    if (isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
      $base_url = $_SERVER["HTTP_X_FORWARDED_PROTO"] . '://';
    } else {
      $base_url = $_SERVER['REQUEST_SCHEME'] . '://';
    }

    if (isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {
      $base_url .= $_SERVER['HTTP_X_FORWARDED_HOST'];
    } else {
      $base_url .= $_SERVER['HTTP_HOST'];
    }

    
    if(empty($to_print))
    $to_print .= $base_url;
    
    $iter = 0;
    $result_count = count($results);
    
    foreach($results as $result) {
      
      if(isset($result->eid))
      $entity_id = $result->eid;
      else
      $entity_id = NULL;
      
      // tuning for solr which does not have eids but stores it in entity:wisski_individual/eid
      if(empty($result->eid)) {
        if(method_exists($result, "__get")) {
          if(!empty(current($result->__get('entity:wisski_individual/eid')))) {
            $entity_id = current($result->__get('entity:wisski_individual/eid'));
          }
        }
      }
      
      // if we have fields for label, imageids and uri we might be able to load from cache.      
      if(!empty($this->options['field_for_label']) && !empty($this->options['field_for_image_ids']) && !empty($this->options['field_for_uri'])) {
        
        $cache = \Drupal::cache('wisski_iiif_params');
        
        $flabel = "";
        if(!empty($result->__get('entity:wisski_individual/' . $this->options['field_for_label'])))
        $flabel = current($result->__get('entity:wisski_individual/' . $this->options['field_for_label']));
        
        $fimageids = "";
        if(!empty($result->__get('entity:wisski_individual/' .$this->options['field_for_image_ids'])))
        $fimageids = $result->__get('entity:wisski_individual/' .$this->options['field_for_image_ids']);
        
        $furi = "";
        if(!empty($result->__get('entity:wisski_individual/' . $this->options['field_for_uri'])))        
        $furi = current($result->__get('entity:wisski_individual/' . $this->options['field_for_uri']));
        
        // stringify.
        foreach($fimageids as $key => $fimageid) {
          $fimageids[$key] = $fimageid->__toString();
        }
        
        $data = array(
          "eid" => $entity_id,
          "flabel" => $flabel->__toString(),
          "fimageids" => $fimageids,
          "furi" => $furi);
          
          // we write this here so it can be acquired by the iiif manifest in the iip module
          $cache->set($entity_id, $data);
          
        }
        
        if(!empty($entity_id)) {
          $ent_list[] = array("manifestId" => $base_url . "/wisski/navigate/" . $entity_id . "/iiif_manifest");
          if ($result_count > 1) {
            $direct_load_list[] = array( "loadedManifest" => $base_url . "/wisski/navigate/" . $entity_id . "/iiif_manifest", "availableViews" => array( 'ImageView'), "slotAddress" => "row1.column" . ++$iter, "viewType" => "ImageView", "bottomPanel" => false, "sidePanel" => false, "annotationLayer" => false);
          }
          else {
            $direct_load_list[] = array( "loadedManifest" => $base_url . "/wisski/navigate/" . $entity_id . "/iiif_manifest", "availableViews" => array( 'ImageView'), "viewType" => "ImageView", "bottomPanel" => false, "sidePanel" => false, "annotationLayer" => false);
          }
        } else {
          $field_to_load_http_uri_from = $view->field;
          $field_to_load_http_uri_from = array_keys($field_to_load_http_uri_from);
          $field_to_load_http_uri_from = current($field_to_load_http_uri_from);
          
          $result_field = $result->$field_to_load_http_uri_from;
          
          $result_field = current($result_field);
          if(!empty($result_field["value"]))
          $result_field = $result_field["value"];
          else
          $result_field = $result_field[0]["value"];
          
          $ent_list[] = array("manifestId" => $result_field);
          
        }
        
        if(isset($view->attachment_before)) {
          $attachments = $view->attachment_before;
          
          foreach($attachments as $attachment) {
            $subview = $attachment['#view'];
            
            $subview->execute();
            $subcount= count($subview->result);
            
            foreach($subview->result as $res) {
              
              $entity_id = empty($res->eid) ? current($res->__get('entity:wisski_individual/eid')) : $res->eid;
              $ent_list[] = array("manifestId" => $base_url . "/wisski/navigate/" . $entity_id . "/iiif_manifest");
              
              if ($subcount > 1) {
                $direct_load_list[] = array( "loadedManifest" => $base_url . "/wisski/navigate/" . $entity_id . "/iiif_manifest", "availableViews" => array( 'ImageView'), "slotAddress" => "row1.column" . ++$iter, "viewType" => "ImageView", "bottomPanel" => false, "sidePanel" => false, "annotationLayer" => false );
              }
              else {
                $direct_load_list[] = array( "loadedManifest" => $base_url . "/wisski/navigate/" . $entity_id . "/iiif_manifest", "availableViews" => array( 'ImageView'), "viewType" => "ImageView", "bottomPanel" => false, "sidePanel" => false, "annotationLayer" => false );
              }
            }
            
          }
          
          // kill the before because we processed it.
          $view->attachment_before = [];
          
        }
        
        $layout = count($ent_list);
        
        $layout_str = "";
        
        if($layout < 9) {
          $layout_str = "1x" . $layout;
        } else {
          $layout_str = "1x1";
        }
        
        
        $form = array();
        
        $form['#attached']['drupalSettings']['wisski']['mirador']['data'] = $ent_list;
        $form['#attached']['drupalSettings']['wisski']['mirador']['options'] = $this->options;
        $form['#attached']['drupalSettings']['wisski']['mirador']['window_settings'] = json_decode($this->options['window_settings'], TRUE);
        $form['#attached']['drupalSettings']['wisski']['mirador']['layout'] = $layout_str;
        
        if($layout < 9) {
          $form['#attached']['drupalSettings']['wisski']['mirador']['windowObjects'] = $direct_load_list;
        }
        
        $form['#markup'] = '<div id="viewer"></div>';
        $form['#allowed_tags'] = array('div', 'select', 'option','a', 'script');
        $form['#attached']['library'][] = "wisski_mirador/mirador";
        
        return $form;
        
      } 
    } 
  }